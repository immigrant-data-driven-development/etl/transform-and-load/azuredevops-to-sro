package br.nemo.immigrant.ontology.transformaload.sro.stakeholders.exceptions;

public class ProjectPersonStakeholderNotFound extends  RuntimeException{
    public ProjectPersonStakeholderNotFound(String message) {
        super("Not Found: "+message);
    }
}
