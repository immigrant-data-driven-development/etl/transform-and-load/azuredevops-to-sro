    package br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.mappers.jira;

    import br.nemo.immigrant.ontology.entity.base.models.Application;
    import br.nemo.immigrant.ontology.entity.sro.scrumprocess.models.Sprint;
    import br.nemo.immigrant.ontology.transformaload.sro.util.ApplicationUtil;
    import br.nemo.immigrant.ontology.transformaload.sro.util.DateUtil;
    import br.nemo.immigrant.ontology.transformaload.sro.util.Mapper;
    import com.fasterxml.jackson.databind.JsonNode;
    import com.fasterxml.jackson.databind.ObjectMapper;
    import org.apache.commons.codec.digest.DigestUtils;
    import org.springframework.stereotype.Component;

    import java.time.LocalDate;

    @Component("Workitem2SprintJiraMapper")
    public class Workitem2SprintMapper implements Mapper<Sprint> {

    public Sprint map (String element) throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode rootNodeOriginal = objectMapper.readTree(element);

        JsonNode rootNode = rootNodeOriginal.path("raw").path("fields").path("customfield_10020").get(0);

        String name = rootNode.path("name").asText();

        String externalid = rootNode.path("id").asText();

        String internalid = new DigestUtils("SHA3-256").digestAsHex(externalid+rootNodeOriginal.path("project").path("uuid").asText());

        LocalDate startDate = LocalDate.parse(rootNode.path("startDate").asText().substring(0,10));

        LocalDate endDate = LocalDate.parse(rootNode.path("endDate").asText().substring(0,10));

        Application application = ApplicationUtil.create(externalid,internalid,"jira");

        Sprint sprint = Sprint.builder().name(name).
                startDate(startDate).
                endDate(endDate).
                internalId(internalid).build();

        sprint.getApplications().add(application);
        return sprint;

    }
}
