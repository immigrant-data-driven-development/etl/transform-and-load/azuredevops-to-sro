package br.nemo.immigrant.ontology.transformaload.sro.productsprintbacklog.exception;

public class SprintBacklogExceptionNotFound extends RuntimeException{

    public SprintBacklogExceptionNotFound(String message) {
        super(message);
    }
}
