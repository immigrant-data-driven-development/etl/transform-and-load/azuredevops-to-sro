
package br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.listeners.jira.sprint;


import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.exception.ScrumProjectExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.mappers.jira.Interaction2SprintMapper;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.mappers.jira.Project2ScrumProjectElementsMapper;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.services.SprintService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Slf4j
@RequiredArgsConstructor
@Service("Interaction2SprintJiraListener")
@Transactional
public class Interaction2SprintListener {


    @Autowired
    private Interaction2SprintMapper interactionMapper;

    @Autowired
    private Project2ScrumProjectElementsMapper projectMapper;

    @Autowired
    private SprintService service;

    private final KafkaTemplate<String, String> kafkaTemplate;

    /** Create a Sprint and a Sprint Backlog **/
    @KafkaListener(topics = "application.jira.sprints", groupId = "interaction2sprint-jira-group", concurrency = "2")
    public void consume(ConsumerRecord<String, String> payload) {

        try{

            this.service.process(payload, interactionMapper,projectMapper );

        }
        catch (ScrumProjectExceptionNotFound e ){
            log.error("Interaction2SprintListener:ScrumProjectExceptionNotFound "+e.getMessage());
           kafkaTemplate.send("application.jira.sprints", payload.value());

        }
        catch (Exception e ){
            log.error("Interaction2SprintListener:Exception "+e.getMessage());
            kafkaTemplate.send("application.jira.sprints.error", payload.value());


        }
    }
}
