package br.nemo.immigrant.ontology.transformaload.sro.productsprintbacklog.mappers.jira;

import br.nemo.immigrant.ontology.entity.base.models.Application;
import br.nemo.immigrant.ontology.entity.spo.artifact.models.ArtifactType;
import br.nemo.immigrant.ontology.entity.sro.productsprintbacklog.models.UserStory;
import br.nemo.immigrant.ontology.entity.sro.productsprintbacklog.models.UserStoryType;
import br.nemo.immigrant.ontology.transformaload.sro.util.ApplicationUtil;
import br.nemo.immigrant.ontology.transformaload.sro.util.DateUtil;
import br.nemo.immigrant.ontology.transformaload.sro.util.Mapper;
import br.nemo.immigrant.ontology.transformaload.sro.util.StringUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.codec.digest.DigestUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public abstract class AbstractWorkitem2UserStoryMapper implements Mapper<UserStory> {

    protected UserStoryType userStoryType;
    
    public AbstractWorkitem2UserStoryMapper(){
        this.userStoryType = UserStoryType.ATOMICUSERSTORY;
    }

    public UserStory map (String element) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode rootNode = objectMapper.readTree(element);

        String externalid = rootNode.path("id").asText();

        String internalid = new DigestUtils("SHA3-256").digestAsHex(externalid+rootNode.path("project").path("uuid").asText());

        String name = StringUtil.check(rootNode.path("key").asText()) + "-"+StringUtil.check(rootNode.path("raw").path("fields").path("summary").asText());

        String description = StringUtil.check(rootNode.path("raw").path("fields").path("description").asText());
        if(description.equals("null")) {description = null;}

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

        ZonedDateTime zonedDateTime = ZonedDateTime.parse(rootNode.path("raw").path("fields").path("created").asText(), formatter);
        LocalDateTime createdDate = zonedDateTime.toLocalDateTime();

        zonedDateTime = ZonedDateTime.parse(rootNode.path("raw").path("fields").path("updated").asText(), formatter);
        LocalDateTime updateDate = zonedDateTime.toLocalDateTime();

        DateTimeFormatter formatterDate = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        String endDateString = rootNode.path("raw").path("fields").path("duedate").asText();
        LocalDate endDate = null;
        try {
            endDate = LocalDate.parse(endDateString, formatterDate);
        } catch (Exception ignore) {}

        String startDateString = rootNode.path("raw").path("fields").path("customfield_10015").asText();
        LocalDate startDate = null;
        try {
            startDate = LocalDate.parse(startDateString, formatterDate);
        } catch (Exception ignore) {}

        Application application = ApplicationUtil.create(externalid,internalid,"jira");

        UserStory userStory =  UserStory.builder().
                internalId(internalid).
                createdDate(createdDate).
                updateDate(updateDate).
                artifacttype(ArtifactType.INFORMATIONITEM).
                userstorytype(this.userStoryType).
                description(description).
                endDate(endDate).
                startDate(startDate).
                name(name).build();

        userStory.getApplications().add(application);
        return userStory;
    }

}
