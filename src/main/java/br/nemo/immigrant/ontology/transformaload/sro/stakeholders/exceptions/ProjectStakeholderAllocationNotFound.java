package br.nemo.immigrant.ontology.transformaload.sro.stakeholders.exceptions;

public class ProjectStakeholderAllocationNotFound extends RuntimeException {

    public ProjectStakeholderAllocationNotFound(String message) {
        super("Project Stakeholder Allocation not found: " + message);
    }
}