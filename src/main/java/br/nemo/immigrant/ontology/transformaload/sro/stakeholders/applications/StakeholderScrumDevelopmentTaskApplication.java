package br.nemo.immigrant.ontology.transformaload.sro.stakeholders.applications;


import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

import br.nemo.immigrant.ontology.entity.sro.stakeholder.models.StakeholderScrumDevelopmentTask;
import br.nemo.immigrant.ontology.entity.sro.stakeholder.repositories.StakeholderScrumDevelopmentTaskRepository;
@Component
@Transactional
public class StakeholderScrumDevelopmentTaskApplication {
    @Autowired
    private StakeholderScrumDevelopmentTaskRepository repository;

    public StakeholderScrumDevelopmentTask create (StakeholderScrumDevelopmentTask stakeholderScrumDevelopmentTask){

        return this.repository.save(stakeholderScrumDevelopmentTask);
    }

}
