    package br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.mappers.jira;

    import br.nemo.immigrant.ontology.entity.base.models.Application;
    import br.nemo.immigrant.ontology.entity.sro.scrumprocess.models.ScrumDevelopmentTask;
    import br.nemo.immigrant.ontology.transformaload.sro.util.ApplicationUtil;
    import br.nemo.immigrant.ontology.transformaload.sro.util.DateUtil;
    import br.nemo.immigrant.ontology.transformaload.sro.util.Mapper;
    import br.nemo.immigrant.ontology.transformaload.sro.util.StringUtil;
    import com.fasterxml.jackson.databind.JsonNode;
    import com.fasterxml.jackson.databind.ObjectMapper;
    import org.apache.commons.codec.digest.DigestUtils;
    import org.springframework.stereotype.Component;

    import java.time.LocalDate;
    import java.time.LocalDateTime;
    import java.time.ZonedDateTime;
    import java.time.format.DateTimeFormatter;

    @Component
    public class Issue2TaskMapper implements Mapper<ScrumDevelopmentTask> {

    public ScrumDevelopmentTask map (String element) throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode rootNode = objectMapper.readTree(element);

        String externalid = rootNode.path("id").asText();

        String internalid = new DigestUtils("SHA3-256").digestAsHex(externalid+rootNode.path("project").path("uuid").asText());

        String name = StringUtil.check(rootNode.path("key").asText()) + "-"+StringUtil.check(rootNode.path("raw").path("fields").path("summary").asText());

        String description = StringUtil.check(rootNode.path("raw").path("fields").path("description").asText());
        if(description.equals("null")) {description = null;}

        String priority = StringUtil.check(rootNode.path("raw").path("fields").path("priority").path("name").asText());

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

        ZonedDateTime zonedDateTime = ZonedDateTime.parse(rootNode.path("raw").path("fields").path("created").asText(), formatter);
        LocalDateTime createdDate = zonedDateTime.toLocalDateTime();

        zonedDateTime = ZonedDateTime.parse(rootNode.path("raw").path("fields").path("updated").asText(), formatter);
        LocalDateTime updateDate = zonedDateTime.toLocalDateTime();

        DateTimeFormatter formatterDate = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        String endDateString = rootNode.path("raw").path("fields").path("duedate").asText();
        LocalDate endDate = null;
        try {
            endDate = LocalDate.parse(endDateString, formatterDate);
        } catch (Exception ignore) {}

        String startDateString = rootNode.path("raw").path("fields").path("customfield_10015").asText();
        LocalDate startDate = null;
        try {
            startDate = LocalDate.parse(startDateString, formatterDate);
        } catch (Exception ignore) {}

        Application application = ApplicationUtil.create(externalid,internalid,"jira");

        ScrumDevelopmentTask scrumDevelopmentTask = ScrumDevelopmentTask.builder().
                internalId(internalid).
                name(name).
                description(description).
                createdDate(createdDate).
                updatedDate(updateDate).
                priority(priority).
                endDate(endDate).
                startDate(startDate).
                build();

        scrumDevelopmentTask.getApplications().add(application);
        return scrumDevelopmentTask;
    }
}
