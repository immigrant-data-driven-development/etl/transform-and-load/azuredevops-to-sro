package br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.exception;

public class ScrumProjectExceptionNotFound extends RuntimeException{

    public ScrumProjectExceptionNotFound(String message) {
        super(message);
    }
}
