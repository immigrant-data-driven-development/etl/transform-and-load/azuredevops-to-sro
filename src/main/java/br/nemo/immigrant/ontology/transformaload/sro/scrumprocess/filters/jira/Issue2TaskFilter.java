    package br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.filters.jira;

    import com.fasterxml.jackson.databind.JsonNode;
    import com.fasterxml.jackson.databind.ObjectMapper;
    import org.springframework.stereotype.Component;

    @Component
    public class Issue2TaskFilter {

    public Boolean isValid (String element) throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode rootNode = objectMapper.readTree(element);

        String issueType = rootNode.path("raw").path("fields").path("issuetype").path("name").asText();

        return (issueType.equals("Tarefa") || issueType.equals("Subtarefa")
                )? true : false;


    }
}
