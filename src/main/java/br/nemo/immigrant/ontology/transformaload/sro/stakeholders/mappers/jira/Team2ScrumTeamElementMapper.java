    package br.nemo.immigrant.ontology.transformaload.sro.stakeholders.mappers.jira;

    import br.nemo.immigrant.ontology.entity.base.models.Application;
    import br.nemo.immigrant.ontology.entity.eo.teams.models.Team;
    import br.nemo.immigrant.ontology.transformaload.sro.util.ApplicationUtil;
    import br.nemo.immigrant.ontology.transformaload.sro.util.Mapper;
    import com.fasterxml.jackson.databind.JsonNode;
    import com.fasterxml.jackson.databind.ObjectMapper;
    import org.apache.commons.codec.digest.DigestUtils;
    import org.springframework.stereotype.Component;

    @Component("Team2ScrumTeamElementJiraMapper")
    public class Team2ScrumTeamElementMapper implements Mapper<Team> {

    public Team map (String element) throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode rootNode = objectMapper.readTree(element);

        if (rootNode.has("project")) {
            rootNode = rootNode.path("project");
        }

        String externalid = rootNode.path("uuid").asText();

        String name = rootNode.path("name").asText();

        String projectID = rootNode.path("id").asText();

        String internalid = new DigestUtils("SHA3-256").digestAsHex(name+projectID);

        Application application = ApplicationUtil.create(externalid,internalid,"jira");
        
        Team team = Team.builder().internalId(internalid).name(name).build();
        
        team.getApplications().add(application);
        
        return team;
    }
}
