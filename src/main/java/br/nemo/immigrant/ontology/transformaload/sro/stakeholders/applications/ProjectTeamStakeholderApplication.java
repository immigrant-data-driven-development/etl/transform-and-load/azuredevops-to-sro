package br.nemo.immigrant.ontology.transformaload.sro.stakeholders.applications;

import br.nemo.immigrant.ontology.entity.spo.stakeholder.models.ProjectTeamStakeholder;
import br.nemo.immigrant.ontology.entity.eo.teams.models.Team;
import org.springframework.beans.factory.annotation.Autowired;
import br.nemo.immigrant.ontology.entity.spo.stakeholder.repositories.ProjectTeamStakeholderRepository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Component;
import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;
@Component
@Transactional
public class ProjectTeamStakeholderApplication {

    @Autowired
    private ProjectTeamStakeholderRepository projectTeamStakeholderRepository;

    public ProjectTeamStakeholder createByTeam (Team  team){

        ProjectTeamStakeholder projectTeamStakeholder = ProjectTeamStakeholder.builder().team(team).internalId(team.getInternalId()).build();
        return this.projectTeamStakeholderRepository.save(projectTeamStakeholder);
    }
}
