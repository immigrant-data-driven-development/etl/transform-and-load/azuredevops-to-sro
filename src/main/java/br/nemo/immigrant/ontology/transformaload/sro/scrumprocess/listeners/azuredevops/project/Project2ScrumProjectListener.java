package br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.listeners.azuredevops.project;

import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.filters.azuredevops.Project2ScrumProjectFilter;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.mappers.azuredevops.Project2ScrumProjectMapper;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.services.ScrumProjectService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service
public class Project2ScrumProjectListener {


    @Autowired
    private Project2ScrumProjectFilter filter;

    @Autowired
    private Project2ScrumProjectMapper mapper;

    @Autowired
    private ScrumProjectService service;

    private final KafkaTemplate<String, String> kafkaTemplate;

    /** Create a Scrum Project **/
    @KafkaListener(topics = "application.msazuredevops.project", groupId = "project2scrumproject-group", concurrency = "2")
    public void consume(ConsumerRecord<String, String> payload) {

        try{

            String data = payload.value();

            if (filter.isValid(data)){

                this.service.process(payload, mapper);
            }

        }catch (Exception e ){
            log.error("Project2ScrumProjectListener:Exception "+e.getMessage());
            kafkaTemplate.send("application.msazuredevops.project.error", payload.value());
        }
    }
}
