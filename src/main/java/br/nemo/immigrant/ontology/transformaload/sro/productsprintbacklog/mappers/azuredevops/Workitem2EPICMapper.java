    package br.nemo.immigrant.ontology.transformaload.sro.productsprintbacklog.mappers.azuredevops;

    
    import br.nemo.immigrant.ontology.entity.sro.productsprintbacklog.models.UserStory;
    import br.nemo.immigrant.ontology.entity.spo.artifact.models.ArtifactType;
    import br.nemo.immigrant.ontology.entity.sro.productsprintbacklog.models.UserStoryType;
    import org.springframework.stereotype.Component;

    @Component
    public class Workitem2EPICMapper extends AbstractWorkitem2UserStoryMapper {

        public  Workitem2EPICMapper(){
            this.userStoryType = UserStoryType.EPIC;
        }

    }

