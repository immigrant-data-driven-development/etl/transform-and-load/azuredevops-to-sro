    package br.nemo.immigrant.ontology.transformaload.sro.productsprintbacklog.filters.azuredevops;

    import br.nemo.immigrant.ontology.entity.sro.productsprintbacklog.models.UserStory;

    import com.fasterxml.jackson.databind.JsonNode;
    import com.fasterxml.jackson.databind.ObjectMapper;
    import lombok.extern.slf4j.Slf4j;
    import org.springframework.stereotype.Component;

    import java.util.Map;

    @Component
    @Slf4j
    public class WorkitemProductUserStory2AtomicUserStoryFilter {

    public Boolean isValid (String element) throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode rootNode = objectMapper.readTree(element);

        log.info("Element: {}", rootNode.path("fields").path("System.WorkItemType").asText());

        return (rootNode.path("fields").path("System.WorkItemType").asText().equals("Product Backlog Item")
                )? true : false;


    }
}
