package br.nemo.immigrant.ontology.transformaload.sro.productsprintbacklog.exception;

public class ProductBacklogExceptionNotFound extends RuntimeException{

    public ProductBacklogExceptionNotFound(String message) {
        super(message);
    }
}
