package br.nemo.immigrant.ontology.transformaload.sro.stakeholders.applications;

import br.nemo.immigrant.ontology.entity.spo.stakeholder.models.ProjectStakeholderAllocation;
import br.nemo.immigrant.ontology.entity.spo.stakeholder.repositories.ProjectStakeholderAllocationRepository;
import br.nemo.immigrant.ontology.transformaload.sro.stakeholders.exceptions.ProjectStakeholderAllocationNotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import java.util.Optional;

@Component
@Transactional
public class ProjectStakeholderAllocationApplication {

    @Autowired
    private ProjectStakeholderAllocationRepository projectStakeholderAllocationRepository;

    public ProjectStakeholderAllocation create(ProjectStakeholderAllocation allocation) {

//        ProjectStakeholderAllocation allocation = ProjectStakeholderAllocation.builder()
//                .projectstakeholder(projectStakeholder)
//                .activity(activity)
//                .plannedDuration(plannedDuration)
//                .internalId(internalid)
//                .build();

        return this.projectStakeholderAllocationRepository.save(allocation);
    }

    public ProjectStakeholderAllocation retrieveByInternalId(String internalId) throws ProjectStakeholderAllocationNotFound {

        Optional<ProjectStakeholderAllocation> result = this.projectStakeholderAllocationRepository.findByInternalId(internalId);

        return result.orElseThrow(() -> new ProjectStakeholderAllocationNotFound(internalId));
    }

    public Boolean exists(String internalId) {
        return this.projectStakeholderAllocationRepository.existsByInternalId(internalId);
    }
}
