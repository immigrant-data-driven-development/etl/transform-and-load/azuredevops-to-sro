    package br.nemo.immigrant.ontology.transformaload.sro.productsprintbacklog.filters.jira;

    import com.fasterxml.jackson.databind.JsonNode;
    import com.fasterxml.jackson.databind.ObjectMapper;
    import lombok.extern.slf4j.Slf4j;
    import org.springframework.stereotype.Component;

    @Component
    @Slf4j
    public class Issue2EPICFilter {

    public Boolean isValid (String element) throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode rootNode = objectMapper.readTree(element);

        log.info("Element: {}", rootNode.path("raw").path("fields").path("issuetype").path("name").asText());

        return (rootNode.path("raw").path("fields").path("issuetype").path("name").asText().equals("Epic")
                )? true : false;


    }
}
