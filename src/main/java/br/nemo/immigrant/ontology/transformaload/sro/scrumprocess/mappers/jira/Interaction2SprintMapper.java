    package br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.mappers.jira;

    import br.nemo.immigrant.ontology.entity.base.models.Application;
    import br.nemo.immigrant.ontology.entity.sro.scrumprocess.models.Sprint;
    import br.nemo.immigrant.ontology.transformaload.sro.util.ApplicationUtil;
    import br.nemo.immigrant.ontology.transformaload.sro.util.DateUtil;
    import br.nemo.immigrant.ontology.transformaload.sro.util.Mapper;
    import com.fasterxml.jackson.databind.JsonNode;
    import com.fasterxml.jackson.databind.ObjectMapper;
    import org.apache.commons.codec.digest.DigestUtils;
    import org.springframework.stereotype.Component;


    import java.time.LocalDate;

    @Component("Interaction2SprintJiraMapper")
    public class Interaction2SprintMapper implements Mapper<Sprint> {

    public Sprint map (String element) throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode rootNode = objectMapper.readTree(element);

        String name = rootNode.path("name").asText();

        String url = rootNode.path("self").asText();

        String externalid = rootNode.path("id").asText();

        String internalid = new DigestUtils("SHA3-256").digestAsHex(externalid+rootNode.path("project").path("uuid").asText());

        LocalDate startDate = LocalDate.parse(rootNode.path("startDate").asText().substring(0,10));

        LocalDate endDate = LocalDate.parse(rootNode.path("endDate").asText().substring(0,10));

        Application application = ApplicationUtil.create(externalid,internalid,"jira");

        Sprint sprint = Sprint.builder().name(name).
                url(url).
                startDate(startDate).
                endDate(endDate).
                internalId(internalid).build();

        sprint.getApplications().add(application);
        return sprint;

    }
}
