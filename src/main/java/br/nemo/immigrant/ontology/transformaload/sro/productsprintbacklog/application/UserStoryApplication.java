package br.nemo.immigrant.ontology.transformaload.sro.productsprintbacklog.application;

import java.util.Optional;
import br.nemo.immigrant.ontology.entity.sro.productsprintbacklog.models.UserStory;
import br.nemo.immigrant.ontology.entity.sro.productsprintbacklog.repositories.UserStoryRepository;
import br.nemo.immigrant.ontology.entity.sro.scrumprocess.models.ScrumDevelopmentTask;
import br.nemo.immigrant.ontology.transformaload.sro.productsprintbacklog.exception.UserStorytExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.exception.ScrumDevelopmentTaskExceptionNotFound;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import  br.nemo.immigrant.ontology.entity.spo.artifact.models.ArtifactArtifact;
import br.nemo.immigrant.ontology.entity.spo.artifact.repositories.ArtifactArtifactRepository;

import org.apache.commons.codec.digest.DigestUtils;
import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;
@Component
@Transactional
public class UserStoryApplication {


    @Autowired
    private  UserStoryRepository repository;

    @Autowired
    private ArtifactArtifactRepository artifactArtifactRepository;

    public UserStory create (UserStory userStory){

        return this.repository.save(userStory);

    }

    public Boolean exists (String internalID){
       return  this.repository.existsByInternalId(internalID);

    }

    private UserStory createInstance(IDProjection projection){
        return  UserStory.builder().id(projection.getId()).applications(projection.getApplications()).internalId(projection.getInternalId()).name(projection.getName()).build();
    }

    public UserStory update(UserStory userStory) throws UserStorytExceptionNotFound {
        UserStory existingUserStory = this.retrieve(userStory.getInternalId());
        Optional<UserStory> optionalUserStory = this.repository.findById(existingUserStory.getId());

        if (optionalUserStory.isEmpty()) {
            throw new RuntimeException("US not found with ID: " + existingUserStory.getId());
        }

        existingUserStory = optionalUserStory.get();

        existingUserStory.setDescription(userStory.getDescription());
        existingUserStory.setName(userStory.getName());
        existingUserStory.setUpdateDate(userStory.getUpdateDate());
        existingUserStory.setCreatedDate(userStory.getCreatedDate());
        existingUserStory.setArtifacttype(userStory.getArtifacttype());
        existingUserStory.setStartDate(userStory.getStartDate());
        existingUserStory.setEndDate(userStory.getEndDate());
        existingUserStory.setUserstorytype(userStory.getUserstorytype());

        existingUserStory = this.repository.save(existingUserStory);

        return existingUserStory;
    }

    public UserStory retrieve(String internalId) throws UserStorytExceptionNotFound {
        Optional<IDProjection> result = this.repository.findByInternalId(internalId);

        IDProjection projection = result.orElseThrow(() -> new UserStorytExceptionNotFound(internalId));

        return createInstance(projection);
    }

    public void addUserStory (String userStoryParentInternalID, String userStoryInternalID) throws UserStorytExceptionNotFound {



        //userStoryParentExternalID = new DigestUtils("SHA3-256").digestAsHex(userStoryParentExternalID);
        //userStoryExternalID = new DigestUtils("SHA3-256").digestAsHex(userStoryExternalID);

        UserStory userStoryParent = this.retrieve(userStoryParentInternalID);
        UserStory userStory = this.retrieve(userStoryInternalID);

        Boolean exists = artifactArtifactRepository.existsByArtifactfromAndArtifactto(userStoryParent,userStory);

        if (!exists){
            ArtifactArtifact artifactArtifact = ArtifactArtifact.builder().artifactfrom(userStoryParent).artifactto(userStory).build();
            this.artifactArtifactRepository.save (artifactArtifact);

        }
        //this.repository.addUserStory(userStory.getId(),userStoryParent);
        //return this.repository.save(scrumDevelopmentTask);
    }
}
