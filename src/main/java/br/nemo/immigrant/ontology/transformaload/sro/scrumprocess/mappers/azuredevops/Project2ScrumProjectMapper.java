    package br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.mappers.azuredevops;

    import br.nemo.immigrant.ontology.entity.sro.scrumprocess.models.ScrumProject;
    import br.nemo.immigrant.ontology.entity.base.models.Application;
    import br.nemo.immigrant.ontology.transformaload.sro.util.ApplicationUtil;
    import br.nemo.immigrant.ontology.transformaload.sro.util.DateUtil;
    import br.nemo.immigrant.ontology.transformaload.sro.util.Mapper;
    import com.fasterxml.jackson.databind.JsonNode;
    import com.fasterxml.jackson.databind.ObjectMapper;
    import org.springframework.cglib.core.Local;
    import org.springframework.stereotype.Component;
    import org.apache.commons.codec.digest.DigestUtils;

    import java.time.LocalDate;
    import java.time.LocalDateTime;
    import java.util.HashSet;
    import java.util.Set;
    @Component
    public class Project2ScrumProjectMapper  implements Mapper<ScrumProject> {

    public ScrumProject map (String element) throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode rootNode = objectMapper.readTree(element);

        String name = rootNode.path("name").asText();

        String description = rootNode.path("description").asText();

        String externalid = rootNode.path("id").asText();

        LocalDateTime createdDate = DateUtil.createLocalDate(
                rootNode.path("last_update_time").path("day").asText(),
                rootNode.path("last_update_time").path("month").asText(),
                rootNode.path("last_update_time").path("year").asText()).atTime(0,0);

        String internalid = new DigestUtils("SHA3-256").digestAsHex(name);

        String url = rootNode.path("url").asText();


        Application application = ApplicationUtil.create(externalid,internalid,"azuredevops");

        ScrumProject scrumProject = ScrumProject.builder().name(name).
                description(description).
                internalId(internalid).
                createdDate(createdDate).
                url(url).
                build();

        scrumProject.getApplications().add(application);

        return scrumProject;
    }
}
