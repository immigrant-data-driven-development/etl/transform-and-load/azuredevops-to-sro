    package br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.mappers.azuredevops;

    import br.nemo.immigrant.ontology.entity.sro.scrumprocess.models.Sprint;
    import br.nemo.immigrant.ontology.transformaload.sro.util.ApplicationUtil;
    import br.nemo.immigrant.ontology.transformaload.sro.util.DateUtil;
    import br.nemo.immigrant.ontology.transformaload.sro.util.Mapper;
    import br.nemo.immigrant.ontology.entity.base.models.Application;
    import com.fasterxml.jackson.databind.JsonNode;
    import com.fasterxml.jackson.databind.ObjectMapper;
    import org.springframework.stereotype.Component;
    import org.apache.commons.codec.digest.DigestUtils;
    import br.nemo.immigrant.ontology.entity.base.models.Application;
    import java.time.LocalDate;

    @Component
    public class Interaction2SprintMapper  implements Mapper<Sprint> {

    public Sprint map (String element) throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode rootNode = objectMapper.readTree(element);

        String name = rootNode.path("path").asText();

        String url = rootNode.path("url").asText();

        String externalid = rootNode.path("id").asText();

        String internalid = new DigestUtils("SHA3-256").digestAsHex(externalid);

        LocalDate startDate = DateUtil.createLocalDate(
                rootNode.path("attributes").path("start_date").path("day").asText(),
                rootNode.path("attributes").path("start_date").path("month").asText(),
                rootNode.path("attributes").path("start_date").path("year").asText()
        );

        LocalDate endDate = DateUtil.createLocalDate(
                rootNode.path("attributes").path("finish_date").path("day").asText(),
                rootNode.path("attributes").path("finish_date").path("month").asText(),
                rootNode.path("attributes").path("finish_date").path("year").asText()
        );

        Application application = ApplicationUtil.create(externalid,internalid,"azuredevops");

        Sprint sprint = Sprint.builder().name(name).
                url(url).
                startDate(startDate).
                endDate(endDate).
                internalId(internalid).build();

        sprint.getApplications().add(application);
        return sprint;

    }
}
