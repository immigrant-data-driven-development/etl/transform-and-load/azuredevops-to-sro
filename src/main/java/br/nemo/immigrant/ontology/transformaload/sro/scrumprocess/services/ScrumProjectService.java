
package br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.services;


import br.nemo.immigrant.ontology.entity.eo.teams.models.Team;
import br.nemo.immigrant.ontology.entity.sro.scrumprocess.models.ScrumProject;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.application.ScrumProjectApplication;
import br.nemo.immigrant.ontology.transformaload.sro.util.Mapper;
import br.nemo.immigrant.ontology.transformaload.sro.stakeholders.services.ScrumTeamService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component
public class ScrumProjectService {


    @Autowired
    private ScrumProjectApplication scrumProjectApplication;

    @Autowired
    ScrumTeamService scrumTeamService;

    public void process(ConsumerRecord<String, String> payload, Mapper<ScrumProject> mapper, Mapper<Team> teamMapper) throws Exception {


        ScrumProject scrumProject = this.process(payload,mapper);

        Team scrumTeam = teamMapper.map(payload.value());

        scrumTeamService.createOrUpdateAndRetrieve(scrumTeam, scrumProject);

    }

    public ScrumProject process(ConsumerRecord<String, String> payload, Mapper<ScrumProject> mapper) throws Exception {

        ScrumProject scrumProject = mapper.map(payload.value());

        return this.createOrUpdateAndRetrieve(scrumProject);

    }

    public ScrumProject createAndRetrieve(ScrumProject scrumProject){

        Boolean exist = this.scrumProjectApplication.exists(scrumProject.getInternalId());

        if (!exist){
            return this.scrumProjectApplication.create(scrumProject);
        }
        else{
            return this.scrumProjectApplication.retrieve(scrumProject.getInternalId());
        }
    }

    public ScrumProject createOrUpdateAndRetrieve(ScrumProject scrumProject){

        Boolean exist = this.scrumProjectApplication.exists(scrumProject.getInternalId());

        if (!exist){
            return this.scrumProjectApplication.create(scrumProject);
        }
        else{
            return this.scrumProjectApplication.update(scrumProject);
        }
    }

}
