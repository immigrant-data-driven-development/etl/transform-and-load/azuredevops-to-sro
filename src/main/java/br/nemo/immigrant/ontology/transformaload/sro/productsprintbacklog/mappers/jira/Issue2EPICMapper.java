    package br.nemo.immigrant.ontology.transformaload.sro.productsprintbacklog.mappers.jira;


    import br.nemo.immigrant.ontology.entity.sro.productsprintbacklog.models.UserStoryType;
    import org.springframework.stereotype.Component;

    @Component
    public class Issue2EPICMapper extends AbstractWorkitem2UserStoryMapper {

        public Issue2EPICMapper(){
            this.userStoryType = UserStoryType.EPIC;
        }

    }

