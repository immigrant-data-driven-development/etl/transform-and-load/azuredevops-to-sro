
package br.nemo.immigrant.ontology.transformaload.sro.productsprintbacklog.listeners.azuredevops;

import br.nemo.immigrant.ontology.entity.sro.productsprintbacklog.models.UserStory;
import br.nemo.immigrant.ontology.transformaload.sro.productsprintbacklog.exception.ProductBacklogExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.sro.productsprintbacklog.services.UserStoryService;
import br.nemo.immigrant.ontology.transformaload.sro.productsprintbacklog.filters.azuredevops.WorkitemProductUserStory2AtomicUserStoryFilter;
import br.nemo.immigrant.ontology.transformaload.sro.productsprintbacklog.mappers.azuredevops.WorkitemProductUserStory2AtomicUserStoryMapper;

import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.exception.ScrumProjectExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.mappers.azuredevops.Project2ScrumProjectElementsMapper;
import br.nemo.immigrant.ontology.transformaload.sro.stakeholders.mappers.azuredevops.Workitem2PersonMapper;
import br.nemo.immigrant.ontology.transformaload.sro.stakeholders.mappers.azuredevops.Workitem2TeamMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.json.simple.JSONObject;
@Slf4j
@RequiredArgsConstructor
@Service
@Transactional
public class WorkitemProductUserStory2AtomicUserStoryListener {


    @Autowired
    private WorkitemProductUserStory2AtomicUserStoryFilter filter;

    @Autowired
    private WorkitemProductUserStory2AtomicUserStoryMapper mapper;

    @Autowired
    private Workitem2PersonMapper personMapper;

    @Autowired
    private Workitem2TeamMapper teamMapper;

    @Autowired
    private Project2ScrumProjectElementsMapper projectMapper;


    @Autowired
    private UserStoryService service;

    private final KafkaTemplate<String, String> kafkaTemplate;

    /** Create a User Story Atomic from Workitem type Product User Story **/
    @KafkaListener(topics = "application.msazuredevops.workitem", groupId = "workitemproductuserstory2atomicuserstory-group", concurrency = "2")
    public void consume(ConsumerRecord<String, String> payload) {

        try{

            String data = payload.value();

            if (filter.isValid(data)){

                this.service.process(payload,personMapper,teamMapper, projectMapper, mapper);
                kafkaTemplate.send("application.msazuredevops.workitem.userstory.related.userstory", data);
            }

        }catch (ProductBacklogExceptionNotFound e){

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("type", "ProductBacklogExceptionNotFound");
            jsonObject.put("error", e.getMessage());
            jsonObject.put("value", payload.value());

            String jsonString = jsonObject.toJSONString();

            kafkaTemplate.send("application.msazuredevops.workitem.productuserstory.error", jsonString);


        }catch (ScrumProjectExceptionNotFound e){

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("type", "ScrumProjectExceptionNotFound");
            jsonObject.put("error", e.getMessage());
            jsonObject.put("value", payload.value());

            String jsonString = jsonObject.toJSONString();

            kafkaTemplate.send("application.msazuredevops.workitem.productuserstory.error", jsonString);

        }
        catch (Exception e ){

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("type", "Exception");
            jsonObject.put("error", e.getMessage());
            jsonObject.put("value", payload.value());

            String jsonString = jsonObject.toJSONString();

            kafkaTemplate.send("application.msazuredevops.workitem.productuserstory.error", jsonString);

        }
    }
}
