
package br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.listeners.azuredevops.workitem;


import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.filters.azuredevops.Workitem2TaskFilter;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.mappers.azuredevops.Project2ScrumProjectElementsMapper;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.mappers.azuredevops.Workitem2SprintMapper;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.mappers.azuredevops.Workitem2TaskMapper;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.services.ScrumDevelopmentTaskService;

import br.nemo.immigrant.ontology.transformaload.sro.stakeholders.mappers.azuredevops.Workitem2PersonMapper;
import br.nemo.immigrant.ontology.transformaload.sro.stakeholders.mappers.azuredevops.Workitem2TeamMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.json.simple.JSONObject;
@Slf4j
@RequiredArgsConstructor
@Service
@Transactional
public class Workitem2TaskListener {


    @Autowired
    private Workitem2TaskFilter filter;

    @Autowired
    private Workitem2TaskMapper mapper;

    @Autowired
    private Workitem2PersonMapper personMapper;

    @Autowired
    private Workitem2TeamMapper teamMapper;

    @Autowired
    private Workitem2SprintMapper sprintMapper;

    @Autowired
    private Project2ScrumProjectElementsMapper projectMapper;


    @Autowired
    private ScrumDevelopmentTaskService service;

    private final KafkaTemplate<String, String> kafkaTemplate;

    /** Creating a Development Task**/
    @KafkaListener(topics = "application.msazuredevops.workitem", groupId = "workitem2task-group", concurrency = "2")
    public void consume(ConsumerRecord<String, String> payload) {

        try{

            String data = payload.value();

            if (filter.isValid(data)){

                this.service.process(payload, personMapper,teamMapper, projectMapper, mapper, sprintMapper);
                kafkaTemplate.send("application.msazuredevops.workitem.scrumdevelopmenttask.related.userstory", data);

            }

        }
        catch (Exception e ){

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("type", "Exception");
            jsonObject.put("error", e.getMessage());
            jsonObject.put("value", payload.value());

            String jsonString = jsonObject.toJSONString();

            kafkaTemplate.send("application.msazuredevops.workitem.task.error", jsonString);


        }
    }
}
