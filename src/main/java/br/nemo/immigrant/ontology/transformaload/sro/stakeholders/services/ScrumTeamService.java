
package br.nemo.immigrant.ontology.transformaload.sro.stakeholders.services;


import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.application.ScrumProjectApplication;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.exception.ScrumProjectExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.services.ScrumProjectService;
import br.nemo.immigrant.ontology.transformaload.sro.stakeholders.applications.ProjectTeamStakeholderApplication;
import br.nemo.immigrant.ontology.transformaload.sro.stakeholders.applications.TeamApplication;
import br.nemo.immigrant.ontology.transformaload.sro.stakeholders.exceptions.TeamExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.sro.util.Mapper;
import br.nemo.immigrant.ontology.entity.eo.teams.models.Team;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import br.nemo.immigrant.ontology.entity.sro.scrumprocess.models.ScrumProject;

@Slf4j
@RequiredArgsConstructor
@Component

public class ScrumTeamService {

    @Autowired
    TeamApplication teamApplication;

    @Autowired
    ScrumProjectApplication scrumProjectApplication;

    @Autowired
    ProjectTeamStakeholderApplication projectTeamStakeholderApplication;

    public void process(ConsumerRecord<String, String> payload, Mapper<Team> teamMapper, Mapper<ScrumProject> scrumMapper) throws JsonProcessingException, ScrumProjectExceptionNotFound, Exception {

       Team team = teamMapper.map(payload.value());

       ScrumProject scrumProject = scrumMapper.map(payload.value());
       scrumProject = scrumProjectApplication.retrieve(scrumProject.getInternalId());
       this.createOrUpdateAndRetrieve(team, scrumProject);

    }

    public Team createAndRetrieve(Team team, ScrumProject scrumProject ) throws TeamExceptionNotFound {
        scrumProject = scrumProjectApplication.retrieve(scrumProject.getInternalId());
        Boolean exists = teamApplication.exists(team.getInternalId());
        if (!exists) {
            team.setProject(scrumProject);
            team = this.teamApplication.create(team);

            this.projectTeamStakeholderApplication.createByTeam(team);
        }
        else {
            team = this.teamApplication.retrieveByInternalId(team.getInternalId());
        }
        return team;
    }

    public Team createOrUpdateAndRetrieve(Team team, ScrumProject scrumProject ) throws TeamExceptionNotFound {
        scrumProject = scrumProjectApplication.retrieve(scrumProject.getInternalId());
        Boolean exists = teamApplication.exists(team.getInternalId());
        if (!exists) {
            team.setProject(scrumProject);
            team = this.teamApplication.create(team);

            this.projectTeamStakeholderApplication.createByTeam(team);
        }
        else {
            team = this.teamApplication.update(team);
        }
        return team;
    }

}
