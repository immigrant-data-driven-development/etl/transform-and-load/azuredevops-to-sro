package br.nemo.immigrant.ontology.transformaload.sro.stakeholders.applications;

import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.application.ScrumProjectApplication;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.exception.ScrumProjectExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.sro.stakeholders.exceptions.PersonExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.sro.stakeholders.exceptions.ProjectPersonStakeholderNotFound;
import org.springframework.beans.factory.annotation.Autowired;
import br.nemo.immigrant.ontology.entity.eo.teams.models.Person;
import org.apache.commons.codec.digest.DigestUtils;
import br.nemo.immigrant.ontology.entity.eo.teams.repositories.PersonRepository;

import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;
import br.nemo.immigrant.ontology.entity.base.projections.IDOnlyProjection;
import br.nemo.immigrant.ontology.entity.spo.stakeholder.models.ProjectPersonStakeholder;
import br.nemo.immigrant.ontology.entity.spo.stakeholder.models.ProjectStakeholderSoftwareProject;
import br.nemo.immigrant.ontology.entity.spo.stakeholder.repositories.ProjectPersonStakeholderRepository;
import br.nemo.immigrant.ontology.entity.spo.stakeholder.repositories.ProjectStakeholderSoftwareProjectRepository;
import br.nemo.immigrant.ontology.entity.sro.scrumprocess.models.ScrumProject;
import br.nemo.immigrant.ontology.entity.sro.scrumprocess.repositories.ScrumProjectRepository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@Transactional
public class ProjectPersonStakeholderApplication {

    @Autowired
    ProjectPersonStakeholderRepository projectPersonStakeholderRepository;

    @Autowired
    PersonApplication personApplication;

    @Autowired
    ScrumProjectApplication scrumProjectApplication;

    @Autowired
    ProjectStakeholderSoftwareProjectRepository projectStakeholderSoftwareProjectRepository;

    public ProjectPersonStakeholder create (Person person, ScrumProject scrumProject) throws Exception {

        String internalid = new DigestUtils("SHA3-256").digestAsHex(person.getName()+scrumProject.getExternalId("jira"));

        ProjectPersonStakeholder projectPersonStakeholder = ProjectPersonStakeholder.
                builder().
                person(person).
                internalId(internalid).
                build();

        projectPersonStakeholder = this.projectPersonStakeholderRepository.save(projectPersonStakeholder);
        
        //Criando o Project Stakeholder    --
        ProjectStakeholderSoftwareProject projectStakeholderSoftwareProject = ProjectStakeholderSoftwareProject.builder().
                projectstakeholder(projectPersonStakeholder)
                .softwareproject(scrumProject).
                internalId(internalid).
                build();
        this.projectStakeholderSoftwareProjectRepository.save(projectStakeholderSoftwareProject);

        return projectPersonStakeholder;
    }

    public ProjectPersonStakeholder retrieveProjectPersonStakeholder(String personExternalID, String projectName) throws PersonExceptionNotFound, ScrumProjectExceptionNotFound, ProjectPersonStakeholderNotFound {

        Person person = this.personApplication.retrieve(personExternalID);

        ScrumProject scrumProject = this.scrumProjectApplication.retrieveByName(projectName);

        Optional<IDOnlyProjection> result = this.projectPersonStakeholderRepository.findFirstByPersonAndProjectstakeholdersoftwareprojectSoftwareproject(person,scrumProject);

        IDOnlyProjection iDProjection = result.orElseThrow(() -> new ProjectPersonStakeholderNotFound(personExternalID +"-"+ projectName));

        return ProjectPersonStakeholder.builder().id(iDProjection.getId()).build();

    }

    public ProjectPersonStakeholder retrieveByInternalId(String internalId){

        Optional<IDOnlyProjection> result = this.projectPersonStakeholderRepository.findByInternalId(internalId);

        IDOnlyProjection iDProjection = result.orElseThrow(() -> new ProjectPersonStakeholderNotFound(internalId));

        return ProjectPersonStakeholder.builder().id(iDProjection.getId()).build();

    }

    public Boolean exists (String internalId){
        return this.projectPersonStakeholderRepository.existsByInternalId(internalId);

    }
}
