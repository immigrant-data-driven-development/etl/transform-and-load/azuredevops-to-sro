    package br.nemo.immigrant.ontology.transformaload.sro.stakeholders.mappers.azuredevops;

    import br.nemo.immigrant.ontology.entity.base.models.Application;
    import br.nemo.immigrant.ontology.entity.eo.teams.models.Team;
    import br.nemo.immigrant.ontology.transformaload.sro.util.ApplicationUtil;
    import br.nemo.immigrant.ontology.transformaload.sro.util.Mapper;
    import com.fasterxml.jackson.databind.JsonNode;
    import com.fasterxml.jackson.databind.ObjectMapper;
    import org.apache.commons.codec.digest.DigestUtils;
    import org.springframework.stereotype.Component;

    @Component
    public class Team2ScrumTeamElementMapper implements Mapper<Team> {

    public Team map (String element) throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode rootNode = objectMapper.readTree(element);

        String externalid = rootNode.path("team").path("id").asText();

        String name = rootNode.path("team").path("name").asText();

        String description = rootNode.path("team").path("description").asText();

        String projectID = rootNode.path("project").path("id").asText();

        String internalid = new DigestUtils("SHA3-256").digestAsHex(name+projectID);

        Application application = ApplicationUtil.create(externalid,internalid,"azuredevops");
        
        Team team = Team.builder().internalId(internalid).name(name).description(description).build();
        
        team.getApplications().add(application);
        
        return team;
    }
}
