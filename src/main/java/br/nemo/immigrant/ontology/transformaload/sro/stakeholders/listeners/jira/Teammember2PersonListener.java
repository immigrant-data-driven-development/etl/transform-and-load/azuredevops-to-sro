
package br.nemo.immigrant.ontology.transformaload.sro.stakeholders.listeners.jira;

import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.mappers.jira.Project2ScrumProjectElementsMapper;
import br.nemo.immigrant.ontology.transformaload.sro.stakeholders.mappers.jira.Team2ScrumTeamElementMapper;
import br.nemo.immigrant.ontology.transformaload.sro.stakeholders.mappers.jira.Teammember2PersonMapper;
import br.nemo.immigrant.ontology.transformaload.sro.stakeholders.services.PersonService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@RequiredArgsConstructor
@Service("Teammember2PersonJiraListener")
@Transactional
public class Teammember2PersonListener {

    @Autowired
    private Teammember2PersonMapper teammeberMapper;

    @Autowired
    private Team2ScrumTeamElementMapper teamMapper;

    @Autowired
    private Project2ScrumProjectElementsMapper projectMapper;

    @Autowired
    private PersonService service;

    private final KafkaTemplate<String, String> kafkaTemplate;

    /** Create a Person and Teammember Based on Team Member **/
    @KafkaListener(topics = "application.jira.users", groupId = "teammember2person-jira-group", concurrency = "2")
    public void consume(ConsumerRecord<String, String> payload) {

        try{

            String data = payload.value();
            this.service.process(payload, teammeberMapper,teamMapper, projectMapper );



        } catch (Exception e ){

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("type", "Exception");
            jsonObject.put("error", e.getMessage());
            jsonObject.put("value", payload.value());

            String jsonString = jsonObject.toJSONString();

            kafkaTemplate.send("application.jira.users.error", jsonString);

        }
    }
}
