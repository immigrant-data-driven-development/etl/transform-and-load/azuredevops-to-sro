package br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.application;


import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;
import br.nemo.immigrant.ontology.entity.sro.scrumprocess.models.ScrumDevelopmentTask;
import br.nemo.immigrant.ontology.entity.sro.scrumprocess.models.Sprint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import br.nemo.immigrant.ontology.entity.spo.process.repositories.SpecificProjectProcessActivityRepository;
import br.nemo.immigrant.ontology.entity.spo.process.models.SpecificProjectProcessActivity;

import java.time.LocalDateTime;
import java.util.Optional;

@Component
@Transactional
public class SprintScrumDevelopmentTaskApplication {

    @Autowired
    private SpecificProjectProcessActivityRepository repository;

    public SpecificProjectProcessActivity create (ScrumDevelopmentTask scrumDevelopmentTask, Sprint sprint, LocalDateTime insertedDate){

        SpecificProjectProcessActivity specificProjectProcessActivity = SpecificProjectProcessActivity.builder().specificprojectprocess(sprint).activity(scrumDevelopmentTask).eventDate(insertedDate).build();
        return this.repository.save(specificProjectProcessActivity);

    }

    public boolean exists (ScrumDevelopmentTask scrumDevelopmentTask, Sprint sprint){
        return this.repository.existsBySpecificprojectprocessAndActivity(sprint,scrumDevelopmentTask);
    }

}
