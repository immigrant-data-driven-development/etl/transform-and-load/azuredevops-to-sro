package br.nemo.immigrant.ontology.transformaload.sro.productsprintbacklog.application;

import  br.nemo.immigrant.ontology.entity.spo.artifact.models.Artifact;
import br.nemo.immigrant.ontology.entity.spo.stakeholder.models.ProjectPersonStakeholder;

import br.nemo.immigrant.ontology.entity.spo.stakeholder.models.ProjectStakeholderArtifact;
import br.nemo.immigrant.ontology.entity.sro.stakeholder.repositories.StakeholderUserStoryRepository;
import br.nemo.immigrant.ontology.entity.sro.stakeholder.models.StakeholderUserStory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import  br.nemo.immigrant.ontology.entity.sro.scrumprocess.models.ScrumDevelopmentTaskEvent;
import java.time.LocalDateTime;
import java.util.Optional;
@Component
@Transactional
public class ProjectStakeholderArtifactApplication {

    @Autowired
    private StakeholderUserStoryRepository repository;
    public StakeholderUserStory create (ProjectPersonStakeholder projectPersonStakeholder, Artifact artifact, LocalDateTime eventDate, ScrumDevelopmentTaskEvent event, String state){

        StakeholderUserStory projectStakeholderArtifact = StakeholderUserStory.builder().projectstakeholder(projectPersonStakeholder).artifact(artifact).state(state).event(event).eventDate(eventDate).build();
        return this.repository.save(projectStakeholderArtifact);

    }
}
