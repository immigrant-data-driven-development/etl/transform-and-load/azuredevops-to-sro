package br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.application;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import br.nemo.immigrant.ontology.entity.sro.scrumprocess.models.ScrumDevelopmentTask;
import br.nemo.immigrant.ontology.entity.sro.scrumprocess.models.ScrumProject;
import br.nemo.immigrant.ontology.entity.base.models.Application;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.exception.ScrumDevelopmentTaskExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.exception.ScrumProjectExceptionNotFound;
import br.nemo.immigrant.ontology.entity.eo.teams.models.Team;
import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;
import br.nemo.immigrant.ontology.entity.sro.scrumprocess.models.ScrumProcess;
import br.nemo.immigrant.ontology.entity.sro.productsprintbacklog.models.ProductBacklog;
import br.nemo.immigrant.ontology.entity.sro.scrumprocess.models.ProductBacklogDefinition;
import br.nemo.immigrant.ontology.entity.spo.artifact.models.ArtifactType;
import br.nemo.immigrant.ontology.entity.spo.process.models.SpecificProjectProcessArtifact;

import br.nemo.immigrant.ontology.entity.sro.scrumprocess.repositories.ScrumProcessRepository;
import br.nemo.immigrant.ontology.entity.sro.scrumprocess.repositories.ProductBacklogDefinitionRepository;
import br.nemo.immigrant.ontology.entity.sro.scrumprocess.repositories.ScrumProjectRepository;
import br.nemo.immigrant.ontology.entity.sro.productsprintbacklog.repositories.ProductBacklogRepository;
import br.nemo.immigrant.ontology.entity.spo.process.repositories.SpecificProjectProcessArtifactRepository;

import br.nemo.immigrant.ontology.transformaload.sro.util.ApplicationUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
@Component
@Transactional
public class ScrumProjectApplication {
    @Autowired
    private ScrumProjectRepository repository;

    @Autowired
    private ScrumProcessRepository scrumprocessRepository;

    @Autowired
    private ProductBacklogDefinitionRepository productBacklogDefinitionRepository;

    @Autowired
    private ProductBacklogRepository productBacklogRepository;

    @Autowired
    private SpecificProjectProcessArtifactRepository specificProjectProcessArtifactRepository;


    public ScrumProject create (ScrumProject scrumProject ){

        List<Application> applications = new ArrayList<>(scrumProject.getApplications());

        Application productBacklogApplication = ApplicationUtil.create(applications.get(0));
        Application productBacklogDefinitionApplication = ApplicationUtil.create(applications.get(0));
        Application scrumprocessApplication = ApplicationUtil.create(applications.get(0));

        scrumProject =  this.repository.save(scrumProject);

        ProductBacklog productBacklog = ProductBacklog.builder().name(scrumProject.getName()).internalId(scrumProject.getInternalId()).artifacttype(ArtifactType.DOCUMENT).build();

        productBacklog.getApplications().add(productBacklogApplication);
        productBacklog =  productBacklogRepository.save(productBacklog);

        ProductBacklogDefinition productBacklogDefinition = ProductBacklogDefinition.builder().name(scrumProject.getName()).internalId(scrumProject.getInternalId()).build();
        productBacklogDefinition.getApplications().add(productBacklogDefinitionApplication);
        productBacklogDefinition  = this.productBacklogDefinitionRepository.save(productBacklogDefinition);

        SpecificProjectProcessArtifact specificProjectProcessArtifact = SpecificProjectProcessArtifact.builder().artifact(productBacklog).specificprojectprocess(productBacklogDefinition).build();

        this.specificProjectProcessArtifactRepository.save(specificProjectProcessArtifact);

        ScrumProcess scrumprocess = ScrumProcess.builder().name(scrumProject.getName()).internalId(scrumProject.getInternalId()).build();
        scrumprocess.getApplications().add(scrumprocessApplication);
        scrumprocess.setSoftwareproject(scrumProject);
        scrumprocess.setProductbacklogdefinition (productBacklogDefinition);

        this.scrumprocessRepository.save(scrumprocess);

        return scrumProject;

    }

    public ScrumProject update(ScrumProject scrumProject){
        ScrumProject existingScrumProject = this.retrieve(scrumProject.getInternalId());
        Optional<ScrumProject> optionalProject = this.repository.findById(existingScrumProject.getId());

        if (optionalProject.isEmpty()) {
            throw new RuntimeException("Project not found with ID: " + existingScrumProject.getId());
        }

        existingScrumProject = optionalProject.get();

        existingScrumProject.setName(scrumProject.getName());

        existingScrumProject = this.repository.save(existingScrumProject);

        return existingScrumProject;
    }

    private ScrumProject createInstance (IDProjection projection){
        return ScrumProject.builder().id(projection.getId()).applications(projection.getApplications()).internalId(projection.getInternalId()).name(projection.getName()).build();
    }

    public ScrumProject retrieveByName(String projecName) throws ScrumProjectExceptionNotFound {
        Optional<IDProjection> result = this.repository.findByName(projecName);

        IDProjection projection = result.orElseThrow(() -> new ScrumProjectExceptionNotFound("Project Not Found: "+projecName));

        return createInstance(projection);
    }

    public ScrumProject retrieve(String internalId) throws ScrumProjectExceptionNotFound {
        Optional<IDProjection> result = this.repository.findByInternalId(internalId);

        IDProjection projection = result.orElseThrow(() -> new ScrumProjectExceptionNotFound("Project Not Found: "+internalId));

        return createInstance(projection);
    }

    public Boolean exists (String internalId){
        return this.repository.existsByInternalId(internalId);
    }

    public String retrieveExternalIdBasedAdditionalProperties (String payload) throws JsonProcessingException {

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode rootNode = objectMapper.readTree(payload);

        return rootNode.path("project").path("id").asText();
    }


}
