
package br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.services;

import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.application.ScrumProjectApplication;
import br.nemo.immigrant.ontology.entity.sro.scrumprocess.models.ScrumProject;
import br.nemo.immigrant.ontology.entity.sro.scrumprocess.models.Sprint;

import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.application.SprintApplication;

import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.exception.ScrumProjectExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.sro.util.Mapper;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component
public class SprintService {

    @Autowired
    private SprintApplication sprintApplication;

    @Autowired
    private ScrumProjectApplication scrumProjectApplication;

    @Autowired
    private ScrumProjectService scrumProjectService;


   public void process(ConsumerRecord<String, String> payload, Mapper<Sprint> sprintMapper, Mapper<ScrumProject> scrumProjectMapper) throws ScrumProjectExceptionNotFound,  Exception {


       String element = payload.value();

       Sprint sprint = sprintMapper.map(element);

       ScrumProject scrumProject = scrumProjectMapper.map(element);

       scrumProject = scrumProjectService.createAndRetrieve(scrumProject);

       this.createOrUpdateAndRetrieve(sprint,scrumProject);

    }

    public Sprint createAndRetrieve(Sprint sprint, ScrumProject scrumProject) throws Exception {

        Boolean exists = this.sprintApplication.exists(sprint.getInternalId());

        if (!exists){

            return this.sprintApplication.create(sprint, scrumProject);
        }
        else{
            return this.sprintApplication.retriveByInternalId(sprint.getInternalId());
        }
    }

    public Sprint createOrUpdateAndRetrieve(Sprint sprint, ScrumProject scrumProject) throws Exception {

        Boolean exists = this.sprintApplication.exists(sprint.getInternalId());

        if (!exists){

            return this.sprintApplication.create(sprint, scrumProject);
        }
        else{
            return this.sprintApplication.update(sprint);
        }
    }

}
