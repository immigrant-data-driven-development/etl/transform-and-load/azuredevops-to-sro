package br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.listeners.jira.project;

import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.mappers.jira.Project2ScrumProjectMapper;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.services.ScrumProjectService;
import br.nemo.immigrant.ontology.transformaload.sro.stakeholders.mappers.jira.Team2ScrumTeamElementMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("Project2ScrumProjectJiraListener")
public class Project2ScrumProjectListener {


    @Autowired
    private Project2ScrumProjectMapper mapper;

    @Autowired
    private Team2ScrumTeamElementMapper teamMapper;

    @Autowired
    private ScrumProjectService service;

    private final KafkaTemplate<String, String> kafkaTemplate;

    /** Create a Scrum Project **/
    @KafkaListener(topics = "application.jira.project", groupId = "project2scrumproject-jira-group", concurrency = "2")
    public void consume(ConsumerRecord<String, String> payload) {

        try{

            this.service.process(payload, mapper, teamMapper);

        }catch (Exception e ){
            log.error("Project2ScrumProjectListener:Exception "+e.getMessage());
            kafkaTemplate.send("application.jira.project.error", payload.value());
        }
    }
}
