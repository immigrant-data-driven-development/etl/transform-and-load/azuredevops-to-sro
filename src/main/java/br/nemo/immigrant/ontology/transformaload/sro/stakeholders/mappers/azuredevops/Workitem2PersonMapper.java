    package br.nemo.immigrant.ontology.transformaload.sro.stakeholders.mappers.azuredevops;

    import br.nemo.immigrant.ontology.entity.base.models.Application;
    import br.nemo.immigrant.ontology.entity.eo.teams.models.Person;
    import br.nemo.immigrant.ontology.transformaload.sro.util.ApplicationUtil;
    import br.nemo.immigrant.ontology.transformaload.sro.util.Mapper;
    import com.fasterxml.jackson.databind.JsonNode;
    import com.fasterxml.jackson.databind.ObjectMapper;
    import org.apache.commons.codec.digest.DigestUtils;
    import org.springframework.stereotype.Component;

    import java.util.ArrayList;
    import java.util.List;

    @Component
    public class Workitem2PersonMapper implements Mapper<List<Person>> {

    public List<Person> map (String element) throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode rootNode = objectMapper.readTree(element);

        List<Person> people = new ArrayList<Person>();

        people.add(this.createPerson("System.CreatedBy", rootNode));
        people.add(this.createPerson("System.AuthorizedAs", rootNode));
        people.add(this.createPerson("System.AssignedTo", rootNode));
        people.add(this.createPerson("System.ChangedBy", rootNode));
        people.add(this.createPerson("System.AssignedTo", rootNode));
        people.add(this.createPerson("Microsoft.VSTS.Common.ActivatedBy", rootNode));
        people.add(this.createPerson("Microsoft.VSTS.Common.ClosedBy", rootNode));
        people.add(this.createPerson("Microsoft.VSTS.Common.ReviewedBy", rootNode));
        people.add(this.createPerson("Microsoft.VSTS.Common.ResolvedBy", rootNode));

        return people;

    }

    private Person createPerson(String pathName, JsonNode rootNode){
            String externalId = rootNode.path("fields").path(pathName).path("id").asText();

            String name = rootNode.path("fields").path(pathName).path("displayName").asText();

            String email = rootNode.path("fields").path(pathName).path("uniqueName").asText();

            String internalid = new DigestUtils("SHA3-256").digestAsHex(name);

            Application application = ApplicationUtil.create(externalId,internalid,"azuredevops");

            Person person = Person.builder().internalId(internalid).name(name).email(email).build();

            person.getApplications().add(application);

            return person;
        }
    }
