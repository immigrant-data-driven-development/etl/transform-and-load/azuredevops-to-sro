
package br.nemo.immigrant.ontology.transformaload.sro.productsprintbacklog.listeners.jira.issues;

import br.nemo.immigrant.ontology.transformaload.sro.productsprintbacklog.exception.ProductBacklogExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.sro.productsprintbacklog.filters.jira.Issue2EPICFilter;
import br.nemo.immigrant.ontology.transformaload.sro.productsprintbacklog.mappers.jira.Issue2EPICMapper;
import br.nemo.immigrant.ontology.transformaload.sro.productsprintbacklog.services.UserStoryService;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.exception.ScrumProjectExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.mappers.jira.Project2ScrumProjectElementsMapper;
import br.nemo.immigrant.ontology.transformaload.sro.stakeholders.mappers.jira.Workitem2PersonMapper;
import br.nemo.immigrant.ontology.transformaload.sro.stakeholders.mappers.jira.Workitem2TeamMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@RequiredArgsConstructor
@Transactional
@Service
public class Issue2EPICJiraListener {


    @Autowired
    private Issue2EPICFilter filter;

    @Autowired
    private Issue2EPICMapper workitemMapper;

    @Autowired
    private Workitem2PersonMapper personMapper;

    @Autowired
    private Workitem2TeamMapper teamMapper;

    @Autowired
    private Project2ScrumProjectElementsMapper projectMapper;

    @Autowired
    private UserStoryService service;

    private final KafkaTemplate<String, String> kafkaTemplate;

    /** Create EPIC from Issue **/
    @KafkaListener(topics = "application.jira.issues", groupId = "issue2epic-group", concurrency = "2")
    public void consume(ConsumerRecord<String, String> payload) {

        try{

            String data = payload.value();
            if (filter.isValid(data)){

                this.service.processJira(payload, personMapper,teamMapper, projectMapper, workitemMapper);

            }

        }
        catch (ProductBacklogExceptionNotFound e){

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("type", "ProductBacklogExceptionNotFound");
            jsonObject.put("error", e.getMessage());
            jsonObject.put("value", payload.value());

            String jsonString = jsonObject.toJSONString();

            kafkaTemplate.send("application.jira.issues.epic.error", jsonString);

        }catch (ScrumProjectExceptionNotFound e){

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("type", "ScrumProjectExceptionNotFound");
            jsonObject.put("error", e.getMessage());
            jsonObject.put("value", payload.value());

            String jsonString = jsonObject.toJSONString();
            kafkaTemplate.send("application.jira.issues.epic.error", jsonString);

        }
        catch (Exception e ){

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("type", "Exception");
            jsonObject.put("error", e.getMessage());
            jsonObject.put("value", payload.value());

            String jsonString = jsonObject.toJSONString();

            kafkaTemplate.send("application.jira.issues.epic.error", jsonString);

        }
    }
}
