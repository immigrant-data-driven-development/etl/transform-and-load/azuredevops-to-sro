package br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.exception;

public class NoSprintException extends RuntimeException{

    public NoSprintException(String message) {
        super(message);
    }
}
