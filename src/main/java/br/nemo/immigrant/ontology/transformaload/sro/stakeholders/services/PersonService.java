
package br.nemo.immigrant.ontology.transformaload.sro.stakeholders.services;

import br.nemo.immigrant.ontology.entity.eo.teams.models.Person;

import br.nemo.immigrant.ontology.entity.eo.teams.models.Team;
import br.nemo.immigrant.ontology.entity.sro.scrumprocess.models.ScrumProject;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.application.ScrumProjectApplication;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.services.ScrumProjectService;
import br.nemo.immigrant.ontology.transformaload.sro.stakeholders.applications.*;
import br.nemo.immigrant.ontology.transformaload.sro.stakeholders.exceptions.TeamExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.sro.util.Mapper;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
@Slf4j
@RequiredArgsConstructor
@Component
public class PersonService {

    @Autowired
    private PersonApplication personApplication;


    @Autowired
    ProjectPersonStakeholderApplication projectPersonStakeholderApplication;

    @Autowired
    private TeamMemberApplication teamMemberApplication;

    @Autowired
    private ScrumProjectService scrumProjectService;

    @Autowired
    private ScrumTeamService scrumTeamService;

    @Autowired
    ScrumProjectApplication scrumProjectApplication;

    @Autowired
    TeamApplication teamApplication;

    public void process(ConsumerRecord<String, String> payload,
                        Mapper<Person> personMapper,
                        Mapper<Team> teamMapper,
                        Mapper<ScrumProject> scrumProjectMapper) throws Exception {

        Person person = personMapper.map(payload.value());

        Team scrumTeam = teamMapper.map(payload.value());
        scrumTeam = teamApplication.retrieveByInternalId(scrumTeam.getInternalId());

        ScrumProject scrumProject = scrumProjectMapper.map(payload.value());
        scrumProject = scrumProjectApplication.retrieve(scrumProject.getInternalId());

        this.doing(person, scrumTeam, scrumProject);

    }

    public void processList(ConsumerRecord<String, String> payload,
                        Mapper<List<Person>> peopleMapper,
                        Mapper<Team> teamMapper,
                        Mapper<ScrumProject> scrumProjectMapper) throws Exception {

        List<Person> people = peopleMapper.map(payload.value());

        Team scrumTeam = teamMapper.map(payload.value());

        ScrumProject scrumProject = scrumProjectMapper.map(payload.value());

        scrumProject = this.scrumProjectService.createAndRetrieve(scrumProject);

        scrumTeam = scrumTeamService.createOrUpdateAndRetrieve(scrumTeam, scrumProject);

        for (Person person:  people){
            this.doing(person, scrumTeam, scrumProject);
        }

    }

    private void doing (Person person,
                        Team scrumTeam,
                        ScrumProject scrumProject) throws TeamExceptionNotFound, Exception {

        createAndRetrieve(person,scrumTeam, scrumProject);


    }

    public Person createAndRetrieve(Person person, Team team, ScrumProject scrumProject) throws Exception {

        Boolean exists = this.personApplication.exists(person.getInternalId());

        if (!exists) {

            //Crinado a pessoa
            person = this.personApplication.create(person);
        }
        else{
            person = this.personApplication.retrieve(person.getInternalId());
        }

        exists = this.teamMemberApplication.exists(person,team);

        if (!exists){
            this.teamMemberApplication.create(person,team);

        }
        String projectPersonStakeholderInternalID = new DigestUtils("SHA3-256").digestAsHex(person.getName()+scrumProject.getExternalId("jira"));

        exists = this.projectPersonStakeholderApplication.exists(projectPersonStakeholderInternalID);

        if (!exists){
            //Criando o Person Stakeholder
            this.projectPersonStakeholderApplication.create(person, scrumProject);

        }

        return person;
    }
}
