package br.nemo.immigrant.ontology.transformaload.sro.productsprintbacklog.mappers.azuredevops;
import br.nemo.immigrant.ontology.entity.sro.productsprintbacklog.models.UserStory;
import br.nemo.immigrant.ontology.entity.spo.artifact.models.ArtifactType;
import br.nemo.immigrant.ontology.entity.sro.productsprintbacklog.models.UserStoryType;
import br.nemo.immigrant.ontology.transformaload.sro.util.ApplicationUtil;
import br.nemo.immigrant.ontology.transformaload.sro.util.DateUtil;
import br.nemo.immigrant.ontology.transformaload.sro.util.Mapper;
import br.nemo.immigrant.ontology.entity.base.models.Application;
import br.nemo.immigrant.ontology.transformaload.sro.util.StringUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.time.LocalDateTime;

import org.apache.commons.codec.digest.DigestUtils;
public abstract class AbstractWorkitem2UserStoryMapper implements Mapper<UserStory> {

    protected UserStoryType userStoryType;
    
    public AbstractWorkitem2UserStoryMapper(){
        this.userStoryType = UserStoryType.ATOMICUSERSTORY;
    }

    public UserStory map (String element) throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode rootNode = objectMapper.readTree(element);

        String externalid = rootNode.path("id").asText();

        String internalid = new DigestUtils("SHA3-256").digestAsHex(externalid);

        String name = StringUtil.check(rootNode.path("fields").path("System.Title").asText());

        String description = StringUtil.check(rootNode.path("fields").path("System.Description").asText());

        String tags = StringUtil.check(rootNode.path("fields").path("System.Tags").asText());

        LocalDateTime createdDate =  DateUtil.createLocalDateTimeZ(rootNode.path("fields").path("System.CreatedDate").asText());

        LocalDateTime updateDate =  DateUtil.createLocalDateTimeZ(rootNode.path("fields").path("System.RevisedDate").asText());

        Application application = ApplicationUtil.create(externalid,internalid,"azuredevops");

        UserStory userStory =  UserStory.builder().
                internalId(internalid).
                tag(tags).
                createdDate(createdDate).
                updateDate(updateDate).
                artifacttype(ArtifactType.INFORMATIONITEM).
                userstorytype(this.userStoryType).
                description(description).
                name(name).build();

        userStory.getApplications().add(application);
        return userStory;
    }

}
