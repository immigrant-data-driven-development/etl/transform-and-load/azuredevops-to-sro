
package br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.listeners.jira.issue;


import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.filters.jira.Issue2TaskFilter;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.mappers.jira.Project2ScrumProjectElementsMapper;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.mappers.jira.Workitem2SprintMapper;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.mappers.jira.Issue2TaskMapper;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.services.ScrumDevelopmentTaskService;
import br.nemo.immigrant.ontology.transformaload.sro.stakeholders.mappers.jira.Workitem2PersonMapper;
import br.nemo.immigrant.ontology.transformaload.sro.stakeholders.mappers.jira.Workitem2TeamMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@RequiredArgsConstructor
@Service("Issue2TaskListener")
@Transactional
public class Issue2TaskListener {


    @Autowired
    private Issue2TaskFilter filter;

    @Autowired
    private Issue2TaskMapper mapper;

    @Autowired
    private Workitem2PersonMapper personMapper;

    @Autowired
    private Workitem2TeamMapper teamMapper;

    @Autowired
    private Workitem2SprintMapper sprintMapper;

    @Autowired
    private Project2ScrumProjectElementsMapper projectMapper;


    @Autowired
    private ScrumDevelopmentTaskService service;

    private final KafkaTemplate<String, String> kafkaTemplate;

    /** Creating a Development Task**/
    @KafkaListener(topics = "application.jira.issues", groupId = "issue2task-group", concurrency = "2")
    public void consume(ConsumerRecord<String, String> payload) {

        try{

            String data = payload.value();

            if (filter.isValid(data)){

                this.service.processJira(payload, personMapper,teamMapper, projectMapper, mapper, sprintMapper);

            }

        }
        catch (Exception e ){

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("type", "Exception");
            jsonObject.put("error", e.getMessage());
            jsonObject.put("value", payload.value());

            String jsonString = jsonObject.toJSONString();

            kafkaTemplate.send("application.jira.issue.task.error", jsonString);


        }
    }
}
