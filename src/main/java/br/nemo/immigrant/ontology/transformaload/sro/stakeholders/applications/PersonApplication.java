package br.nemo.immigrant.ontology.transformaload.sro.stakeholders.applications;

import java.util.Optional;
import br.nemo.immigrant.ontology.entity.eo.teams.models.Person;
import br.nemo.immigrant.ontology.entity.eo.teams.repositories.PersonRepository;
import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;
import br.nemo.immigrant.ontology.transformaload.sro.stakeholders.exceptions.PersonExceptionNotFound;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

@Component
@Transactional
public class PersonApplication {

    @Autowired
    private PersonRepository repository;

    public Person create (Person person){
        return this.repository.save(person);
    }

    public Boolean exists (String internalId){
        return this.repository.existsByInternalId(internalId);
    }

    public Person retrieve(String internalId) throws PersonExceptionNotFound {
        Optional<IDProjection> result = this.repository.findByInternalId(internalId);

        IDProjection projection = result.orElseThrow(() -> new PersonExceptionNotFound(internalId));

        return createInstance(projection);
    }

    private Person createInstance (IDProjection projection){
        return Person.builder().id(projection.getId()).applications(projection.getApplications()).internalId(projection.getInternalId()).name(projection.getName()).build();
    }
}
