
package br.nemo.immigrant.ontology.transformaload.sro.productsprintbacklog.services;

import br.nemo.immigrant.ontology.entity.eo.teams.models.Person;
import br.nemo.immigrant.ontology.entity.eo.teams.models.Team;
import br.nemo.immigrant.ontology.entity.spo.artifact.models.ArtifactType;
import br.nemo.immigrant.ontology.entity.spo.process.models.StateType;
import br.nemo.immigrant.ontology.entity.spo.process.models.SuccessType;
import br.nemo.immigrant.ontology.entity.spo.stakeholder.models.ProjectPersonStakeholder;
import br.nemo.immigrant.ontology.entity.sro.productsprintbacklog.models.SprintBacklog;
import br.nemo.immigrant.ontology.entity.sro.productsprintbacklog.models.UserStory;
import br.nemo.immigrant.ontology.entity.sro.productsprintbacklog.models.ProductBacklog;

import br.nemo.immigrant.ontology.entity.sro.productsprintbacklog.models.UserStoryType;
import br.nemo.immigrant.ontology.entity.sro.scrumprocess.models.ScrumDevelopmentTask;
import br.nemo.immigrant.ontology.entity.sro.scrumprocess.models.ScrumDevelopmentTaskEvent;
import br.nemo.immigrant.ontology.entity.sro.scrumprocess.models.ScrumProject;
import br.nemo.immigrant.ontology.entity.base.models.Application;
import br.nemo.immigrant.ontology.transformaload.sro.productsprintbacklog.application.ProductBacklogApplication;
import br.nemo.immigrant.ontology.transformaload.sro.productsprintbacklog.application.ProjectStakeholderArtifactApplication;
import br.nemo.immigrant.ontology.transformaload.sro.productsprintbacklog.application.SprintBacklogApplication;
import br.nemo.immigrant.ontology.transformaload.sro.productsprintbacklog.application.UserStoryApplication;
import br.nemo.immigrant.ontology.transformaload.sro.productsprintbacklog.exception.UserStorytExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.application.ScrumProjectApplication;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.exception.ScrumDevelopmentTaskExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.exception.ScrumProjectExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.services.ScrumProjectService;
import br.nemo.immigrant.ontology.transformaload.sro.stakeholders.applications.*;
import br.nemo.immigrant.ontology.transformaload.sro.stakeholders.exceptions.PersonExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.sro.stakeholders.exceptions.ProjectPersonStakeholderNotFound;
import br.nemo.immigrant.ontology.transformaload.sro.stakeholders.services.PersonService;
import br.nemo.immigrant.ontology.transformaload.sro.stakeholders.services.ScrumTeamService;
import br.nemo.immigrant.ontology.transformaload.sro.util.ApplicationUtil;
import br.nemo.immigrant.ontology.transformaload.sro.util.DateUtil;
import br.nemo.immigrant.ontology.transformaload.sro.util.Mapper;

import br.nemo.immigrant.ontology.transformaload.sro.productsprintbacklog.exception.ProductBacklogExceptionNotFound;


import br.nemo.immigrant.ontology.transformaload.sro.util.StringUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import org.apache.commons.codec.digest.DigestUtils;
@Slf4j
@RequiredArgsConstructor
@Component
public class UserStoryService {


    @Autowired
    private UserStoryApplication userStoryApplication;


    @Autowired
    ProjectPersonStakeholderApplication projectPersonStakeholderApplication;

    @Autowired
    private ScrumProjectService scrumProjectService;

    @Autowired
    private ScrumTeamService scrumTeamService;

    @Autowired
    private PersonService personService;

    @Autowired
    private ProjectStakeholderArtifactApplication projectStakeholderArtifactApplication;

    @Autowired
    ProductBacklogApplication productBacklogApplication;

    @Autowired
    private SprintBacklogApplication sprintBacklogApplication;

    public void processJira(ConsumerRecord<String, String> payload,
                        Mapper<Person> personMapper,
                        Mapper<Team> teamMapper,
                        Mapper<ScrumProject> scrumProjectMapper,
                        Mapper<UserStory> userStoryMapper) throws ProductBacklogExceptionNotFound, ScrumProjectExceptionNotFound, Exception {

        //Mapeando os elementos
        Person person = personMapper.map(payload.value());

        Team scrumTeam = teamMapper.map(payload.value());


        ScrumProject scrumProject = scrumProjectMapper.map(payload.value());

        UserStory userStory = userStoryMapper.map(payload.value());

        scrumProject = scrumProjectService.createAndRetrieve(scrumProject);

        scrumTeam = scrumTeamService.createOrUpdateAndRetrieve(scrumTeam,scrumProject);

        String data = payload.value();

        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode rootNode = objectMapper.readTree(data);

        personService.createAndRetrieve(person,scrumTeam,scrumProject);

        //Criando sem o PAI
        userStory = createOrUpdateAndRetrieve(userStory);

        //Associando ao Sprint Backlog
        ProductBacklog productBacklog = productBacklogApplication.retriveByInternalId(scrumProject.getInternalId());

        Boolean exists = productBacklogApplication.exists(productBacklog,userStory);

        if (!exists){
            productBacklogApplication.create(userStory,productBacklog, userStory.getCreatedDate());
        }

        //Associar ao Usuario
        createPersonProjectStakeholderUserStoryJira(rootNode, "created", "creator",ScrumDevelopmentTaskEvent.CREATED, userStory,scrumProject );
        createPersonProjectStakeholderUserStoryJira(rootNode, "updated", "assignee",ScrumDevelopmentTaskEvent.ASSIGNED,userStory,scrumProject);

        JsonNode sprintNode = rootNode.path("raw").path("fields").path("customfield_10020");
        // Adicionando o Sprint
        if (sprintNode != null && sprintNode.isArray() && !sprintNode.isEmpty()) {

            String iterationName = StringUtil.check(sprintNode.get(0).path("name").asText());
            String changedDate = StringUtil.check(sprintNode.get(0).path("startDate").asText());

            if (!iterationName.isBlank() || !iterationName.isEmpty()){
                SprintBacklog sprintBacklog = this.sprintBacklogApplication.findByInternalId(new DigestUtils("SHA3-256").digestAsHex(StringUtil.check(sprintNode.get(0).path("id").asText())+rootNode.path("project").path("uuid").asText()));
                LocalDateTime insertedDate = DateUtil.createLocalDateTimeZ(changedDate);
                exists = this.sprintBacklogApplication.exists(userStory, sprintBacklog);

                if (!exists){

                    this.sprintBacklogApplication.create(userStory,sprintBacklog,insertedDate);

                }

            }
        }

        String userStoryParentID = rootNode.path("raw").path("fields").path("parent").path("id").asText();
        String tipoStory = rootNode.path("raw").path("fields").path("parent").path("fields").path("issuetype").path("name").asText();
        UserStoryType userStoryTypeParent;

        if (!userStoryParentID.isBlank() && !userStoryParentID.isEmpty() && (tipoStory.equals("Epic") || tipoStory.equals("História"))){

            String internalid = new DigestUtils("SHA3-256").digestAsHex(userStoryParentID+rootNode.path("project").path("uuid").asText());

            // userStoryMapper.map(objectMapper.writeValueAsString(rootNode.path("raw").path("fields").path("parent")));
            String name = StringUtil.check(rootNode.path("raw").path("fields").path("parent").path("raw").path("key").asText());

            String description = StringUtil.check(rootNode.path("raw").path("fields").path("parent").path("fields").path("summary").asText());

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

            ZonedDateTime zonedDateTime = ZonedDateTime.parse(rootNode.path("raw").path("fields").path("parent").path("raw").path("fields").path("created").asText(), formatter);
            LocalDateTime createdDate = zonedDateTime.toLocalDateTime();

            zonedDateTime = ZonedDateTime.parse(rootNode.path("raw").path("fields").path("parent").path("raw").path("fields").path("updated").asText(), formatter);
            LocalDateTime updateDate = zonedDateTime.toLocalDateTime();

           if (tipoStory.equals("Epic")) {
                userStoryTypeParent  = UserStoryType.EPIC;
            }
           else {
                userStoryTypeParent  = UserStoryType.ATOMICUSERSTORY;
            }

            Application application = ApplicationUtil.create(userStoryParentID,internalid,"jira");

            UserStory userStoryParent = UserStory.builder().
                            internalId(internalid).
                            createdDate(createdDate).
                            updateDate(updateDate).
                            artifacttype(ArtifactType.INFORMATIONITEM).
                            userstorytype(userStoryTypeParent).
                            description(description).
                            name(name).build();



            userStoryParent = createAndRetrieve(userStoryParent);
            String userStoryChild = rootNode.path("raw").path("id").asText();
            this.userStoryApplication.addUserStory(userStoryParent.getInternalId(),userStory.getInternalId());

        }


    }

    public void process(ConsumerRecord<String, String> payload,
                        Mapper<List<Person>> peopleMapper,
                        Mapper<Team> teamMapper,
                        Mapper<ScrumProject> scrumProjectMapper,
                        Mapper<UserStory> userStoryMapper) throws ProductBacklogExceptionNotFound, ScrumProjectExceptionNotFound, Exception {


        List<Person> people = peopleMapper.map(payload.value());

        Team scrumTeam = teamMapper.map(payload.value());

        ScrumProject scrumProject = scrumProjectMapper.map(payload.value());

        UserStory userStory = userStoryMapper.map(payload.value());

        scrumProject = scrumProjectService.createAndRetrieve(scrumProject);

        scrumTeam = scrumTeamService.createOrUpdateAndRetrieve(scrumTeam,scrumProject);

        String data = payload.value();

        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode rootNode = objectMapper.readTree(data);

        for (Person person:  people){

            personService.createAndRetrieve(person,scrumTeam,scrumProject);

        }

        userStory = createAndRetrieve(userStory);

        //Associando ao Sprint Backlog
        ProductBacklog productBacklog = productBacklogApplication.retriveByInternalId(scrumProject.getInternalId());

        Boolean exists = productBacklogApplication.exists(productBacklog,userStory);

        if (!exists){
            productBacklogApplication.create(userStory,productBacklog, userStory.getCreatedDate());
        }

        //Associar ao Usuario

        createPersonProjectStakeholderUserStory(rootNode, "System.CreatedDate", "System.CreatedBy",ScrumDevelopmentTaskEvent.CREATED, userStory,scrumProject );
        createPersonProjectStakeholderUserStory(rootNode, "System.AuthorizedDate", "System.AuthorizedAs",ScrumDevelopmentTaskEvent.AUTHORIZED, userStory,scrumProject);
        createPersonProjectStakeholderUserStory(rootNode, "System.ChangedDate", "System.ChangedBy",ScrumDevelopmentTaskEvent.CHANGED, userStory,scrumProject);
        createPersonProjectStakeholderUserStory(rootNode, "Microsoft.VSTS.Common.StateChangeDate", "System.AssignedTo",ScrumDevelopmentTaskEvent.ASSIGNED,userStory,scrumProject);
        createPersonProjectStakeholderUserStory(rootNode, "Microsoft.VSTS.Common.ActivatedDate", "Microsoft.VSTS.Common.ActivatedBy",ScrumDevelopmentTaskEvent.ACTIVATED,userStory,scrumProject);
        createPersonProjectStakeholderUserStory(rootNode, "Microsoft.VSTS.Common.ClosedDate", "Microsoft.VSTS.Common.ClosedBy",ScrumDevelopmentTaskEvent.CLOSED,userStory,scrumProject);
        createPersonProjectStakeholderUserStory(rootNode, "System.RevisedDate", "Microsoft.VSTS.Common.ReviewedBy",ScrumDevelopmentTaskEvent.REVISED,userStory,scrumProject);
        createPersonProjectStakeholderUserStory(rootNode, "Microsoft.VSTS.Common.ResolvedDate", "Microsoft.VSTS.Common.ResolvedBy",ScrumDevelopmentTaskEvent.RESOLVED,userStory,scrumProject);

        //Associar ao SpringBacklog

        String iterationName = StringUtil.check(rootNode.path("fields").path("System.IterationPath").asText());
        String changedDate = StringUtil.check(rootNode.path("fields").path("System.ChangedDate").asText());

        if (iterationName.contains(String.valueOf('\\'))){


                SprintBacklog sprintBacklog = this.sprintBacklogApplication.findByName(iterationName);
                LocalDateTime insertedDate = DateUtil.createLocalDateTimeZ(changedDate);
                exists = this.sprintBacklogApplication.exists(userStory, sprintBacklog);

                if (!exists){
                    this.sprintBacklogApplication.create(userStory,sprintBacklog,insertedDate);

            }
        }




    }

    public UserStory createAndRetrieve (UserStory userStory) throws UserStorytExceptionNotFound {
        Boolean exists = userStoryApplication.exists(userStory.getInternalId());

        if (!exists){
           return userStoryApplication.create(userStory);
        }
        else{
            return userStoryApplication.retrieve(userStory.getInternalId());
        }
    }

    public UserStory createOrUpdateAndRetrieve(UserStory userStory) throws UserStorytExceptionNotFound {
        Boolean exists = userStoryApplication.exists(userStory.getInternalId());

        if (exists) {
            return userStoryApplication.update(userStory);
        } else {
            return userStoryApplication.create(userStory);
        }
    }

    private void createPersonProjectStakeholderUserStory(JsonNode rootNode, String date, String role, ScrumDevelopmentTaskEvent event,UserStory userStory, ScrumProject scrumProject) throws PersonExceptionNotFound, UserStorytExceptionNotFound, ProjectPersonStakeholderNotFound {

        String personName = rootNode.path("fields").path(role).path("displayName").asText();

        if (!personName.isBlank() || !personName.isEmpty()){

            String researchDate = rootNode.path("fields").path(date).asText();

            String projectPersonStakeholderInternalID = new DigestUtils("SHA3-256").digestAsHex(personName+scrumProject.getName());

            LocalDateTime eventDate = DateUtil.createLocalDateTimeZ(researchDate);

            String state = StringUtil.check( rootNode.path("fields").path(role).path("System.State").asText());

            ProjectPersonStakeholder projectPersonStakeholder = this.projectPersonStakeholderApplication.retrieveByInternalId(projectPersonStakeholderInternalID);

            this.projectStakeholderArtifactApplication.create(projectPersonStakeholder,userStory, eventDate,event,state);

        }
    }

    private void createPersonProjectStakeholderUserStoryJira(JsonNode rootNode, String date, String role, ScrumDevelopmentTaskEvent event,UserStory userStory, ScrumProject scrumProject) throws Exception {

        String personName = rootNode.path("raw").path("fields").path(role).path("displayName").asText();

        if (!personName.isBlank() || !personName.isEmpty()){

            String projectPersonStakeholderInternalID = new DigestUtils("SHA3-256").digestAsHex(personName+scrumProject.getExternalId("jira"));

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

            ZonedDateTime zonedDateTime = ZonedDateTime.parse(rootNode.path("raw").path("fields").path(date).asText(), formatter);
            LocalDateTime eventDate = zonedDateTime.toLocalDateTime();

            String state = StringUtil.check(rootNode.path("raw").path("fields").path("customfield_10020").get(0).path("state").asText());

            ProjectPersonStakeholder projectPersonStakeholder = this.projectPersonStakeholderApplication.retrieveByInternalId(projectPersonStakeholderInternalID);

            this.projectStakeholderArtifactApplication.create(projectPersonStakeholder,userStory, eventDate,event,state);

        }
    }


}
