
package br.nemo.immigrant.ontology.transformaload.sro.stakeholders.listeners.azuredevops;

import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.mappers.azuredevops.Project2ScrumProjectElementsMapper;
import br.nemo.immigrant.ontology.transformaload.sro.stakeholders.mappers.azuredevops.Team2ScrumTeamElementMapper;
import br.nemo.immigrant.ontology.transformaload.sro.stakeholders.services.PersonService;
import br.nemo.immigrant.ontology.transformaload.sro.stakeholders.filters.azuredevops.Teammember2PersonFilter;
import br.nemo.immigrant.ontology.transformaload.sro.stakeholders.mappers.azuredevops.Teammember2PersonMapper;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.json.simple.JSONObject;
@Slf4j
@RequiredArgsConstructor
@Service
@Transactional
public class Teammember2PersonListener {


    @Autowired
    private Teammember2PersonFilter filter;

    @Autowired
    private Teammember2PersonMapper teammeberMapper;

    @Autowired
    private Team2ScrumTeamElementMapper teamMapper;

    @Autowired
    private Project2ScrumProjectElementsMapper projectMapper;

    @Autowired
    private PersonService service;

    private final KafkaTemplate<String, String> kafkaTemplate;

    /** Create a Person and Teammember Based on Team Member **/
    @KafkaListener(topics = "application.msazuredevops.teammember", groupId = "teammember2person-group", concurrency = "2")
    public void consume(ConsumerRecord<String, String> payload) {

        try{

            String data = payload.value();

            if (filter.isValid(data)){

                this.service.process(payload, teammeberMapper,teamMapper, projectMapper );
            }


        } catch (Exception e ){

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("type", "Exception");
            jsonObject.put("error", e.getMessage());
            jsonObject.put("value", payload.value());

            String jsonString = jsonObject.toJSONString();

            kafkaTemplate.send("application.msazuredevops.teammember.error", jsonString);

        }
    }
}
