
package br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.services;
import br.nemo.immigrant.ontology.entity.base.models.Application;
import br.nemo.immigrant.ontology.entity.eo.teams.models.Person;
import br.nemo.immigrant.ontology.entity.eo.teams.models.Team;
import br.nemo.immigrant.ontology.entity.spo.artifact.models.ArtifactType;
import br.nemo.immigrant.ontology.entity.spo.process.models.ActivityEvent;
import br.nemo.immigrant.ontology.entity.spo.process.repositories.ActivityEventRepository;
import br.nemo.immigrant.ontology.entity.spo.stakeholder.models.ProjectPersonStakeholder;
import br.nemo.immigrant.ontology.entity.spo.stakeholder.models.ProjectStakeholderAllocation;
import br.nemo.immigrant.ontology.entity.sro.productsprintbacklog.models.ProductBacklog;
import br.nemo.immigrant.ontology.entity.sro.productsprintbacklog.models.UserStory;
import br.nemo.immigrant.ontology.entity.sro.productsprintbacklog.models.UserStoryType;
import br.nemo.immigrant.ontology.entity.sro.scrumprocess.models.ScrumDevelopmentTaskEvent;
import br.nemo.immigrant.ontology.entity.sro.scrumprocess.models.ScrumProject;
import br.nemo.immigrant.ontology.entity.sro.stakeholder.models.StakeholderScrumDevelopmentTask;
import br.nemo.immigrant.ontology.transformaload.sro.productsprintbacklog.application.ProductBacklogApplication;
import br.nemo.immigrant.ontology.transformaload.sro.productsprintbacklog.services.UserStoryService;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.application.ScrumDevelopmentTaskApplication;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.application.ScrumProjectApplication;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.application.SprintApplication;
import br.nemo.immigrant.ontology.transformaload.sro.stakeholders.applications.ProjectStakeholderAllocationApplication;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.application.SprintScrumDevelopmentTaskApplication;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.exception.NoSprintException;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.exception.ScrumDevelopmentTaskExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.exception.SprintExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.sro.util.ApplicationUtil;
import br.nemo.immigrant.ontology.entity.sro.stakeholder.repositories.StakeholderScrumDevelopmentTaskRepository;
import org.apache.commons.codec.digest.DigestUtils;
import java.util.Optional;
import br.nemo.immigrant.ontology.entity.spo.process.models.StateType;
import br.nemo.immigrant.ontology.entity.spo.process.models.SuccessType;
import br.nemo.immigrant.ontology.entity.sro.scrumprocess.models.Sprint;
import br.nemo.immigrant.ontology.entity.sro.scrumprocess.models.ScrumDevelopmentTask;

import br.nemo.immigrant.ontology.transformaload.sro.stakeholders.applications.*;
import br.nemo.immigrant.ontology.transformaload.sro.stakeholders.exceptions.PersonExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.sro.stakeholders.services.PersonService;
import br.nemo.immigrant.ontology.transformaload.sro.stakeholders.services.ScrumTeamService;
import br.nemo.immigrant.ontology.transformaload.sro.util.DateUtil;
import br.nemo.immigrant.ontology.transformaload.sro.util.Mapper;
import br.nemo.immigrant.ontology.transformaload.sro.util.StringUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Component
public class ScrumDevelopmentTaskService {


    @Autowired
    private ScrumDevelopmentTaskApplication scrumDevelopmentTaskApplication;

    @Autowired
    ProjectPersonStakeholderApplication projectPersonStakeholderApplication;

    @Autowired
    StakeholderScrumDevelopmentTaskApplication stakeholderScrumDevelopmentTaskApplication;

    @Autowired
    ProjectStakeholderAllocationApplication projectStakeholderAllocationApplication;

    @Autowired
    ProductBacklogApplication productBacklogApplication;

    @Autowired
    ScrumProjectService scrumProjectService;

    @Autowired
    ScrumTeamService scrumTeamService;

    @Autowired
    PersonService personService;

    @Autowired
    UserStoryService userStoryService;

    @Autowired
    SprintService sprintService;

    @Autowired
    TeamApplication teamApplication;

    @Autowired
    private StakeholderScrumDevelopmentTaskRepository stakeholderScrumDevelopmentTaskRepository;

    @Autowired
    private ActivityEventRepository activityEventRepository;

    @Autowired
    private SprintScrumDevelopmentTaskApplication sprintScrumDevelopmentTaskApplication;

    public void process(ConsumerRecord<String, String> payload,
                        Mapper<List<Person>> peopleMapper,
                        Mapper<Team> teamMapper,
                        Mapper<ScrumProject> scrumProjectMapper,
                        Mapper<ScrumDevelopmentTask> mapper,
                        Mapper<Sprint> sprintMapper) throws  Exception{

        List<Person> people = peopleMapper.map(payload.value());

        Team scrumTeam = teamMapper.map(payload.value());

        ScrumProject scrumProject = scrumProjectMapper.map(payload.value());

        ScrumDevelopmentTask scrumDevelopmentTask = mapper.map(payload.value());

        Sprint sprint = sprintMapper.map(payload.value());

        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode rootNode = objectMapper.readTree(payload.value());

        scrumProject = scrumProjectService.createAndRetrieve(scrumProject);

        scrumTeam = scrumTeamService.createOrUpdateAndRetrieve(scrumTeam,scrumProject);

        for (Person person:  people){
            personService.createAndRetrieve(person,scrumTeam,scrumProject);
        }

        scrumDevelopmentTask = createAndRetrieve(scrumDevelopmentTask);

        //Assciando a um Product Backlog
        ProductBacklog productBacklog = productBacklogApplication.retriveByInternalId(scrumProject.getInternalId());

        Boolean exists = productBacklogApplication.exists(productBacklog,scrumDevelopmentTask);

        if (!exists){
            productBacklogApplication.create(productBacklog,scrumDevelopmentTask);
        }

        //Asspciar as Pessoas
        createPersonScrumDevelopmentTask(rootNode, "System.CreatedDate", "System.CreatedBy", ScrumDevelopmentTaskEvent.CREATED,scrumDevelopmentTask,scrumProject);
        createPersonScrumDevelopmentTask(rootNode, "System.AuthorizedDate", "System.AuthorizedAs",ScrumDevelopmentTaskEvent.AUTHORIZED,scrumDevelopmentTask,scrumProject);
        createPersonScrumDevelopmentTask(rootNode, "System.ChangedDate", "System.ChangedBy",ScrumDevelopmentTaskEvent.CHANGED,scrumDevelopmentTask,scrumProject);
        createPersonScrumDevelopmentTask(rootNode, "Microsoft.VSTS.Common.StateChangeDate", "System.AssignedTo",ScrumDevelopmentTaskEvent.ASSIGNED,scrumDevelopmentTask,scrumProject);
        createPersonScrumDevelopmentTask(rootNode, "Microsoft.VSTS.Common.ActivatedDate", "Microsoft.VSTS.Common.ActivatedBy",ScrumDevelopmentTaskEvent.ACTIVATED,scrumDevelopmentTask,scrumProject);
        createPersonScrumDevelopmentTask(rootNode, "Microsoft.VSTS.Common.ClosedDate", "Microsoft.VSTS.Common.ClosedBy",ScrumDevelopmentTaskEvent.CLOSED,scrumDevelopmentTask,scrumProject);
        createPersonScrumDevelopmentTask(rootNode, "System.RevisedDate", "Microsoft.VSTS.Common.ReviewedBy",ScrumDevelopmentTaskEvent.REVISED,scrumDevelopmentTask,scrumProject);
        createPersonScrumDevelopmentTask(rootNode, "Microsoft.VSTS.Common.ResolvedDate", "Microsoft.VSTS.Common.ResolvedBy",ScrumDevelopmentTaskEvent.RESOLVED,scrumDevelopmentTask,scrumProject);

        //Adicionando o ScrumDevelopmentTask no sprint
        String iterationName = StringUtil.check(rootNode.path("fields").path("System.IterationPath").asText());
        if (iterationName.contains(String.valueOf('\\'))){
            String changedDate = StringUtil.check(rootNode.path("fields").path("System.ChangedDate").asText());
            sprint = sprintService.createOrUpdateAndRetrieve(sprint,scrumProject);
            exists = this.sprintScrumDevelopmentTaskApplication.exists(scrumDevelopmentTask, sprint);

            if (!exists){
                LocalDateTime insertedDate = DateUtil.createLocalDateTimeZ(changedDate);
                this.sprintScrumDevelopmentTaskApplication.create(scrumDevelopmentTask, sprint,insertedDate);
            }
        }

        //criando a performance Task
        createPerfomedDevelopmentTask(rootNode, "System.ChangedDate", "System.ChangedBy", scrumDevelopmentTask, ScrumDevelopmentTaskEvent.CHANGED);

    }


    public void process(ConsumerRecord<String, String> payload,
                        Mapper<ScrumProject> scrumProjectMapper,
                        Mapper<ScrumDevelopmentTask> mapper,
                        Mapper<Sprint> sprintMapper,
                        Mapper<Person> personMapper) throws  Exception{

        Person person = personMapper.map(payload.value());

        ScrumProject scrumProject = scrumProjectMapper.map(payload.value());

        ScrumDevelopmentTask scrumDevelopmentTask = mapper.map(payload.value());

        Sprint sprint = sprintMapper.map(payload.value());

        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode rootNode = objectMapper.readTree(payload.value());

        scrumProject = scrumProjectService.createAndRetrieve(scrumProject);

        Team scrumTeam = this.teamApplication.retrieve(scrumProject.getInternalId());

        personService.createAndRetrieve(person,scrumTeam,scrumProject);

        scrumDevelopmentTask = createAndRetrieve(scrumDevelopmentTask);

        //Assciando a um Product Backlog
        ProductBacklog productBacklog = productBacklogApplication.retriveByInternalId(scrumProject.getInternalId());

        Boolean exists = productBacklogApplication.exists(productBacklog,scrumDevelopmentTask);

        if (!exists){
            productBacklogApplication.create(productBacklog,scrumDevelopmentTask);
        }

        //Asspciar as Pessoas
        createPersonScrumDevelopmentTask(rootNode, "System.CreatedDate", "System.CreatedBy", ScrumDevelopmentTaskEvent.CREATED,scrumDevelopmentTask,scrumProject);
        createPersonScrumDevelopmentTask(rootNode, "System.AuthorizedDate", "System.AuthorizedAs",ScrumDevelopmentTaskEvent.AUTHORIZED,scrumDevelopmentTask,scrumProject);
        createPersonScrumDevelopmentTask(rootNode, "System.ChangedDate", "System.ChangedBy",ScrumDevelopmentTaskEvent.CHANGED,scrumDevelopmentTask,scrumProject);
        createPersonScrumDevelopmentTask(rootNode, "Microsoft.VSTS.Common.StateChangeDate", "System.AssignedTo",ScrumDevelopmentTaskEvent.ASSIGNED,scrumDevelopmentTask,scrumProject);
        createPersonScrumDevelopmentTask(rootNode, "Microsoft.VSTS.Common.ActivatedDate", "Microsoft.VSTS.Common.ActivatedBy",ScrumDevelopmentTaskEvent.ACTIVATED,scrumDevelopmentTask,scrumProject);
        createPersonScrumDevelopmentTask(rootNode, "Microsoft.VSTS.Common.ClosedDate", "Microsoft.VSTS.Common.ClosedBy",ScrumDevelopmentTaskEvent.CLOSED,scrumDevelopmentTask,scrumProject);
        createPersonScrumDevelopmentTask(rootNode, "System.RevisedDate", "Microsoft.VSTS.Common.ReviewedBy",ScrumDevelopmentTaskEvent.REVISED,scrumDevelopmentTask,scrumProject);
        createPersonScrumDevelopmentTask(rootNode, "Microsoft.VSTS.Common.ResolvedDate", "Microsoft.VSTS.Common.ResolvedBy",ScrumDevelopmentTaskEvent.RESOLVED,scrumDevelopmentTask,scrumProject);

        //Adicionando o ScrumDevelopmentTask no sprint
        String iterationName = StringUtil.check(rootNode.path("fields").path("System.IterationPath").asText());
        if (iterationName.contains(String.valueOf('\\'))){
            String changedDate = StringUtil.check(rootNode.path("fields").path("System.ChangedDate").asText());
            sprint = sprintService.createOrUpdateAndRetrieve(sprint,scrumProject);
            exists = this.sprintScrumDevelopmentTaskApplication.exists(scrumDevelopmentTask, sprint);

            if (!exists){
                LocalDateTime insertedDate = DateUtil.createLocalDateTimeZ(changedDate);
                this.sprintScrumDevelopmentTaskApplication.create(scrumDevelopmentTask, sprint,insertedDate);
            }
        }

        //criando a performance Task
        createPerfomedDevelopmentTask(rootNode, "System.ChangedDate", "System.ChangedBy", scrumDevelopmentTask, ScrumDevelopmentTaskEvent.CHANGED);

    }

    public void processJira(ConsumerRecord<String, String> payload,
                             Mapper<Person> personMapper,
                             Mapper<Team> teamMapper,
                             Mapper<ScrumProject> scrumProjectMapper,
                             Mapper<ScrumDevelopmentTask> mapper,
                             Mapper<Sprint> sprintMapper) throws  Exception{

        Person person = personMapper.map(payload.value());

        Team scrumTeam = teamMapper.map(payload.value());

        ScrumProject scrumProject = scrumProjectMapper.map(payload.value());

        ScrumDevelopmentTask scrumDevelopmentTask = mapper.map(payload.value());
        Sprint sprint = sprintMapper.map(payload.value());

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode rootNode = objectMapper.readTree(payload.value());

        scrumProject = scrumProjectService.createOrUpdateAndRetrieve(scrumProject);
        scrumTeam = this.teamApplication.retrieveByProjectName(scrumProject.getName());
        personService.createAndRetrieve(person,scrumTeam,scrumProject);

        scrumDevelopmentTask = createOrUpdateAndRetrieve(scrumDevelopmentTask);

        //Assciando a um Product Backlog
        ProductBacklog productBacklog = productBacklogApplication.retriveByInternalId(scrumProject.getInternalId());

        Boolean exists = productBacklogApplication.exists(productBacklog,scrumDevelopmentTask);

        if (!exists){
            productBacklogApplication.create(productBacklog,scrumDevelopmentTask);
        }

        //Asspciar as Pessoas
        createPersonScrumDevelopmentTaskJira(rootNode, "created", "creator", ScrumDevelopmentTaskEvent.CREATED,scrumDevelopmentTask,scrumProject);
        createPersonScrumDevelopmentTaskJira(rootNode, "updated", "assignee",ScrumDevelopmentTaskEvent.ASSIGNED,scrumDevelopmentTask,scrumProject);
        //createPersonScrumDevelopmentTask(rootNode, "System.AuthorizedDate", "System.AuthorizedAs",ScrumDevelopmentTaskEvent.AUTHORIZED,scrumDevelopmentTask,scrumProject);
        //createPersonScrumDevelopmentTask(rootNode, "System.ChangedDate", "System.ChangedBy",ScrumDevelopmentTaskEvent.CHANGED,scrumDevelopmentTask,scrumProject);
        //createPersonScrumDevelopmentTask(rootNode, "Microsoft.VSTS.Common.ActivatedDate", "Microsoft.VSTS.Common.ActivatedBy",ScrumDevelopmentTaskEvent.ACTIVATED,scrumDevelopmentTask,scrumProject);
        //createPersonScrumDevelopmentTask(rootNode, "Microsoft.VSTS.Common.ClosedDate", "Microsoft.VSTS.Common.ClosedBy",ScrumDevelopmentTaskEvent.CLOSED,scrumDevelopmentTask,scrumProject);
        //createPersonScrumDevelopmentTask(rootNode, "System.RevisedDate", "Microsoft.VSTS.Common.ReviewedBy",ScrumDevelopmentTaskEvent.REVISED,scrumDevelopmentTask,scrumProject);
        //createPersonScrumDevelopmentTask(rootNode, "Microsoft.VSTS.Common.ResolvedDate", "Microsoft.VSTS.Common.ResolvedBy",ScrumDevelopmentTaskEvent.RESOLVED,scrumDevelopmentTask,scrumProject);

        //Adicionando o ScrumDevelopmentTask no sprint
        JsonNode sprintNode = rootNode.path("raw").path("fields").path("customfield_10020");
        if (sprintNode != null && sprintNode.isArray() && !sprintNode.isEmpty()) {
            String iterationName = StringUtil.check(sprintNode.get(0).path("name").asText());

            if (!iterationName.isBlank() || !iterationName.isEmpty()){

                String changedDate = StringUtil.check(sprintNode.get(0).path("startDate").asText());
                sprint = sprintService.createOrUpdateAndRetrieve(sprint,scrumProject);
                exists = this.sprintScrumDevelopmentTaskApplication.exists(scrumDevelopmentTask, sprint);

                if (!exists){
                    LocalDateTime insertedDate = DateUtil.createLocalDateTimeZ(changedDate);
                    this.sprintScrumDevelopmentTaskApplication.create(scrumDevelopmentTask, sprint,insertedDate);
                }
            }
        }
        //criando a performance Task
        createPerfomedDevelopmentTask(rootNode, scrumDevelopmentTask);

        String userStoryParentID = rootNode.path("raw").path("fields").path("parent").path("id").asText();
        String tipoStory = rootNode.path("raw").path("fields").path("parent").path("raw").path("fields").path("issuetype").path("name").asText();
        UserStoryType userStoryTypeParent;

        if (!userStoryParentID.isBlank() && !userStoryParentID.isEmpty() && (tipoStory.equals("Epic") || tipoStory.equals("História"))){

            String internalid = new DigestUtils("SHA3-256").digestAsHex(userStoryParentID+rootNode.path("project").path("uuid").asText());

            // userStoryMapper.map(objectMapper.writeValueAsString(rootNode.path("raw").path("fields").path("parent")));
            String name = StringUtil.check(rootNode.path("raw").path("fields").path("parent").path("raw").path("key").asText()+"-"+StringUtil.check(rootNode.path("raw").path("fields").path("parent").path("raw").path("fields").path("summary").asText()));

            String description = StringUtil.check(rootNode.path("raw").path("fields").path("parent").path("raw").path("fields").path("summary").asText());

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

            ZonedDateTime zonedDateTime = ZonedDateTime.parse(rootNode.path("raw").path("fields").path("parent").path("raw").path("fields").path("created").asText(), formatter);
            LocalDateTime createdDate = zonedDateTime.toLocalDateTime();

            zonedDateTime = ZonedDateTime.parse(rootNode.path("raw").path("fields").path("parent").path("raw").path("fields").path("updated").asText(), formatter);
            LocalDateTime updateDate = zonedDateTime.toLocalDateTime();

            if (tipoStory.equals("Epic")) {
                userStoryTypeParent  = UserStoryType.EPIC;
            }
            else {
                userStoryTypeParent  = UserStoryType.ATOMICUSERSTORY;
            }

            Application application = ApplicationUtil.create(userStoryParentID,internalid,"jira");

            UserStory userStoryParent = UserStory.builder().
                    internalId(internalid).
                    createdDate(createdDate).
                    updateDate(updateDate).
                    artifacttype(ArtifactType.INFORMATIONITEM).
                    userstorytype(userStoryTypeParent).
                    description(description).
                    name(name).build();



            userStoryParent = userStoryService.createAndRetrieve(userStoryParent);
            this.scrumDevelopmentTaskApplication.addUserStory(userStoryParent.getInternalId(),scrumDevelopmentTask.getInternalId());

        }

    }

    public ScrumDevelopmentTask createAndRetrieve(ScrumDevelopmentTask scrumDevelopmentTask) throws ScrumDevelopmentTaskExceptionNotFound {
       Boolean exists = this.scrumDevelopmentTaskApplication.exists(scrumDevelopmentTask.getInternalId());

        if (!exists){
            return this.scrumDevelopmentTaskApplication.create(scrumDevelopmentTask, StateType.INTENDED,SuccessType.UNDEFINED );
        }
        else{
            return this.scrumDevelopmentTaskApplication.retrieve(scrumDevelopmentTask.getInternalId());
        }
    }

    public ScrumDevelopmentTask createOrUpdateAndRetrieve(ScrumDevelopmentTask scrumDevelopmentTask) throws ScrumDevelopmentTaskExceptionNotFound {
        Boolean exists = this.scrumDevelopmentTaskApplication.exists(scrumDevelopmentTask.getInternalId());

        if (exists) {
            return this.scrumDevelopmentTaskApplication.update(scrumDevelopmentTask);
        } else {
            return this.scrumDevelopmentTaskApplication.create(scrumDevelopmentTask, StateType.INTENDED,SuccessType.UNDEFINED );
        }
    }

    private void createPersonScrumDevelopmentTaskJira(JsonNode rootNode, String date, String role,ScrumDevelopmentTaskEvent event,ScrumDevelopmentTask scrumDevelopmentTask, ScrumProject scrumProject) throws Exception {


        String personName = rootNode.path("raw").path("fields").path(role).path("displayName").asText();

        if (!personName.isEmpty() || !personName.isBlank()){

            String projectPersonStakeholderInternalID = new DigestUtils("SHA3-256").digestAsHex(personName+scrumProject.getExternalId("jira"));
            
            int duration = rootNode.path("raw").path("fields").path("timespent").asInt();

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

            ZonedDateTime zonedDateTime = ZonedDateTime.parse(rootNode.path("raw").path("fields").path(date).asText(), formatter);
            LocalDateTime eventDate = zonedDateTime.toLocalDateTime();

            String state = StringUtil.check(rootNode.path("raw").path("fields").path("customfield_10020").get(0).path("state").asText());
            ProjectPersonStakeholder projectPersonStakeholder = this.projectPersonStakeholderApplication.retrieveByInternalId(projectPersonStakeholderInternalID);

            StakeholderScrumDevelopmentTask stakeholderScrumDevelopmentTask = StakeholderScrumDevelopmentTask.builder().projectstakeholder(projectPersonStakeholder).activity(scrumDevelopmentTask).event(event).state(state).eventDate(eventDate).duration(duration).build();

            Optional<StakeholderScrumDevelopmentTask> existingTask = this.stakeholderScrumDevelopmentTaskRepository.findByActivityAndEvent(scrumDevelopmentTask, event);

            if (existingTask.isEmpty()) {
                this.stakeholderScrumDevelopmentTaskApplication.create(stakeholderScrumDevelopmentTask);

                if (role.equals("assignee")){
                    String allocationInternalId = new DigestUtils("SHA3-256").digestAsHex(projectPersonStakeholder.getInternalId()+scrumDevelopmentTask.getInternalId());

                    if (!projectStakeholderAllocationApplication.exists(allocationInternalId)) {

                        int plannedDuration = rootNode.path("raw").path("fields").path("timeoriginalestimate").asInt();

                        DateTimeFormatter formatterDate = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                        LocalDate temp;

                        String plannedEventDateString = rootNode.path("raw").path("fields").path("duedate").asText();
                        LocalDateTime plannedEventDate = null;
                        try {
                            temp = LocalDate.parse(plannedEventDateString, formatterDate);
                            plannedEventDate = temp.atStartOfDay();
                        } catch (Exception ignore) {}

                        String plannedStartDateString = rootNode.path("raw").path("fields").path("customfield_10015").asText();
                        LocalDateTime plannedStartDate = null;
                        try {
                            temp = LocalDate.parse(plannedStartDateString, formatterDate);
                            plannedStartDate = temp.atStartOfDay();
                        } catch (Exception ignore) {}


                        ProjectStakeholderAllocation allocation = ProjectStakeholderAllocation.builder().projectstakeholder(projectPersonStakeholder).activity(scrumDevelopmentTask).plannedDuration(plannedDuration).plannedEventDate(plannedEventDate).plannedStartDate(plannedStartDate).internalId(allocationInternalId).build();

                        this.projectStakeholderAllocationApplication.create(allocation);

                    }
                }

            }

        }

    }

    private void createPersonScrumDevelopmentTask(JsonNode rootNode, String date, String role, ScrumDevelopmentTaskEvent event,ScrumDevelopmentTask scrumDevelopmentTask, ScrumProject scrumProject) throws PersonExceptionNotFound, ScrumDevelopmentTaskExceptionNotFound {


        String personName = rootNode.path("fields").path(role).path("displayName").asText();

        if (!personName.isEmpty() || !personName.isBlank()){

            String researchDate = StringUtil.check(rootNode.path("fields").path(date).asText());

            String projectPersonStakeholderInternalID = new DigestUtils("SHA3-256").digestAsHex(personName+scrumProject.getName());

            LocalDateTime eventDate = DateUtil.createLocalDateTimeZ(researchDate);

            String state = StringUtil.check(rootNode.path("fields").path("System.State").asText());

            ProjectPersonStakeholder projectPersonStakeholder = this.projectPersonStakeholderApplication.retrieveByInternalId(projectPersonStakeholderInternalID);

            StakeholderScrumDevelopmentTask stakeholderScrumDevelopmentTask = StakeholderScrumDevelopmentTask.builder().projectstakeholder(projectPersonStakeholder).state(state).activity(scrumDevelopmentTask).event(event).eventDate(eventDate).build();

            this.stakeholderScrumDevelopmentTaskApplication.create(stakeholderScrumDevelopmentTask);

        }

    }

    private void createPerfomedDevelopmentTask(JsonNode rootNode, String date, String role, ScrumDevelopmentTask scrumDevelopmentTask, ScrumDevelopmentTaskEvent event) throws ScrumDevelopmentTaskExceptionNotFound{


        String researchDate = rootNode.path("fields").path(date).asText();

        LocalDateTime eventDate = DateUtil.createLocalDateTimeZ(researchDate);

        String state =  rootNode.path("fields").path("System.State").asText();

        if (!state.equals("To Do") && !state.equals("New") && state.equals("Active") && !state.isBlank() && !state.isEmpty()){

            StateType stateType = StateType.PERFORMED;

            SuccessType  successType = SuccessType.UNDEFINED;

            if (state.equals("Done") || state.equals("Closed") ){
                successType = SuccessType.SUCCESSS;
            }

            ActivityEvent activityEvent = ActivityEvent.builder().activity(scrumDevelopmentTask).eventDate(eventDate).statetype(stateType).successtype(successType).build();

            activityEventRepository.save(activityEvent);

        }

    }

    private void createPerfomedDevelopmentTask(JsonNode rootNode, ScrumDevelopmentTask scrumDevelopmentTask) throws ScrumDevelopmentTaskExceptionNotFound{

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

        ZonedDateTime zonedDateTime = ZonedDateTime.parse(rootNode.path("raw").path("fields").path("statuscategorychangedate").asText(), formatter);
        LocalDateTime eventDate = zonedDateTime.toLocalDateTime();

        String state =  rootNode.path("raw").path("fields").path("status").path("statusCategory").path("key").asText();

        if (!state.equals("new") && !state.isBlank() && !state.isEmpty()){

            StateType stateType = StateType.PERFORMED;

            SuccessType  successType = SuccessType.UNDEFINED;

            if (state.equals("done")){
                successType = SuccessType.SUCCESSS;
            }

            Optional<ActivityEvent> existingEvent = activityEventRepository.findByActivityAndStatetypeAndSuccesstype(scrumDevelopmentTask, stateType, successType);

            if (existingEvent.isEmpty()) {
                ActivityEvent activityEvent = ActivityEvent.builder().activity(scrumDevelopmentTask).eventDate(eventDate).statetype(stateType).successtype(successType).build();

                activityEventRepository.save(activityEvent);
            }


        }

    }

}
