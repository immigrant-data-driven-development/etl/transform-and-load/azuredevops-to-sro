package br.nemo.immigrant.ontology.transformaload.sro.stakeholders.exceptions;

public class PersonExceptionNotFound extends RuntimeException{
    public PersonExceptionNotFound(String message) {
        super("Not Found: "+message);
    }
}
