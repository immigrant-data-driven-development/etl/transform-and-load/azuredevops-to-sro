package br.nemo.immigrant.ontology.transformaload.sro.stakeholders.applications;


import br.nemo.immigrant.ontology.entity.eo.teams.models.Team;
import br.nemo.immigrant.ontology.entity.eo.teams.repositories.TeamRepository;

import java.util.Optional;

import br.nemo.immigrant.ontology.entity.sro.scrumprocess.models.ScrumDevelopmentTask;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.exception.ScrumDevelopmentTaskExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.sro.stakeholders.exceptions.TeamExceptionNotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;
@Component
@Transactional
public class TeamApplication {

    @Autowired
    private TeamRepository repository;

    public Team create(Team team){

        return this.repository.save(team);

    }

    public Team update(Team team) throws TeamExceptionNotFound {
        Team existingTeam = this.retrieveByInternalId(team.getInternalId());
        Optional<Team> optionalTeam = this.repository.findById(existingTeam.getId());

        if (optionalTeam.isEmpty()) {
            throw new RuntimeException("Team not found with ID: " + existingTeam.getId());
        }
        existingTeam = optionalTeam.get();

        existingTeam.setDescription(team.getDescription());
        existingTeam.setName(team.getName());

        existingTeam = this.repository.save(existingTeam);

        return existingTeam;
    }

    private Team create (IDProjection teamProjection) {
        return Team.builder().id(teamProjection.getId()).name(teamProjection.getName()).build();
    }
    public Team retrieve(String externalID) throws TeamExceptionNotFound {
        Optional<IDProjection> result = this.repository.findFirstByApplicationsExternalId(externalID);

        IDProjection teamProjection = result.orElseThrow(() -> new TeamExceptionNotFound());

        return this.create(teamProjection);
    }

    public Team retrieveByInternalId(String internalId) throws TeamExceptionNotFound {
        Optional<IDProjection> result = this.repository.findByInternalId(internalId);

        IDProjection teamProjection = result.orElseThrow(() -> new TeamExceptionNotFound());

        return this.create(teamProjection);
    }

    public Boolean exists (String internalId){
        return this.repository.existsByInternalId(internalId);
    }

    public Team retrieveByProjectName(String name) throws TeamExceptionNotFound {
        Optional<IDProjection> result = this.repository.findFirstByProjectName(name);

        IDProjection teamProjection = result.orElseThrow(() -> new TeamExceptionNotFound());

        return this.create(teamProjection);

    
    }

}
