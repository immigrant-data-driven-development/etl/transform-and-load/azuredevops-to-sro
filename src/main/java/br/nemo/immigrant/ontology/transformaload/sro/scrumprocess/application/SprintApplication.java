package br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.application;

import br.nemo.immigrant.ontology.entity.eo.teams.models.Team;
import br.nemo.immigrant.ontology.entity.sro.scrumprocess.models.ScrumProcess;
import br.nemo.immigrant.ontology.entity.sro.scrumprocess.models.Sprint;
import br.nemo.immigrant.ontology.entity.sro.scrumprocess.models.ScrumProject;
import br.nemo.immigrant.ontology.entity.sro.productsprintbacklog.models.SprintBacklog;
import br.nemo.immigrant.ontology.entity.spo.artifact.models.ArtifactType;
import br.nemo.immigrant.ontology.entity.sro.scrumprocess.repositories.SprintRepository;
import br.nemo.immigrant.ontology.entity.sro.productsprintbacklog.repositories.SprintBacklogRepository;
import java.util.Optional;
import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.exception.NoSprintException;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.exception.ScrumProjectExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.exception.SprintExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.sro.stakeholders.exceptions.TeamExceptionNotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import br.nemo.immigrant.ontology.entity.sro.scrumprocess.repositories.ScrumProcessRepository;
import br.nemo.immigrant.ontology.entity.spo.process.repositories.SpecificProjectProcessArtifactRepository;
import br.nemo.immigrant.ontology.entity.spo.process.models.SpecificProjectProcessArtifact;

@Component
@Transactional
public class SprintApplication {
    @Autowired
    private SprintRepository repository;

    @Autowired
    private SprintBacklogRepository sprintBacklogRepository;
    @Autowired
    private ScrumProcessRepository scrumProcessRepository;

    @Autowired
    private SpecificProjectProcessArtifactRepository specificProjectProcessArtifactRepository;

    public Sprint create (Sprint sprint, ScrumProject scrumProject) throws  Exception{

        Optional<IDProjection> result = scrumProcessRepository.findByInternalId(scrumProject.getInternalId());

        IDProjection projection = result.orElseThrow(() -> new ScrumProjectExceptionNotFound("Project Not Found: "+scrumProject.getInternalId()));

        ScrumProcess scrumProcess = ScrumProcess.builder().id(projection.getId()).applications(projection.getApplications()).internalId(projection.getInternalId()).name(projection.getName()).build();

        sprint.setGeneralprojectprocess(scrumProcess);

        SprintBacklog sprintBacklog = SprintBacklog.builder().
                name(sprint.getName()).
                applications(sprint.getApplications()).
                internalId(sprint.getInternalId()).
                artifacttype(ArtifactType.DOCUMENT).
                createdtDate(sprint.getStartDate()).build();

        this.sprintBacklogRepository.save(sprintBacklog);

        sprint.setSprintbacklog(sprintBacklog);

        sprint = this.repository.save(sprint);

        SpecificProjectProcessArtifact specificProjectProcessArtifact = SpecificProjectProcessArtifact.
                builder().
                artifact(sprintBacklog).
                specificprojectprocess(sprint).
                build();

        this.specificProjectProcessArtifactRepository.save(specificProjectProcessArtifact);

        return sprint;
    }

    public Sprint update(Sprint sprint) {
        Sprint existingSprint = this.retriveByInternalId(sprint.getInternalId());
        Optional<Sprint> optionalSprint = this.repository.findById(existingSprint.getId());

        if (optionalSprint.isEmpty()) {
            throw new RuntimeException("Sprint not found with ID: " + existingSprint.getId());
        }
        existingSprint = optionalSprint.get();

        existingSprint.setDescription(sprint.getDescription());
        existingSprint.setName(sprint.getName());

        existingSprint = this.repository.save(existingSprint);

        return existingSprint;
    }

    public Boolean exists (String internalID){
       return this.repository.existsByInternalId(internalID);
    }

    public Sprint retrivebyName (String sprintName) throws SprintExceptionNotFound,NoSprintException {

        if (sprintName.contains(String.valueOf('\\'))){
            Optional<IDProjection> result = this.repository.findByName(sprintName);

            IDProjection projection = result.orElseThrow(() -> new SprintExceptionNotFound(sprintName));
            return Sprint.builder().id(projection.getId()).build();

        }
        throw new NoSprintException(sprintName);
    }

    public Sprint retriveByInternalId (String internalId) throws SprintExceptionNotFound,NoSprintException {

        Optional<IDProjection> result = this.repository.findByInternalId(internalId);

        IDProjection projection = result.orElseThrow(() -> new SprintExceptionNotFound(internalId));
        return Sprint.builder().id(projection.getId()).build();

    }
}



