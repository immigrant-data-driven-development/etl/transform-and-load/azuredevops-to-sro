package br.nemo.immigrant.ontology.transformaload.sro.stakeholders.applications;

import br.nemo.immigrant.ontology.entity.eo.teams.models.Team;
import br.nemo.immigrant.ontology.entity.eo.teams.models.Person;
import br.nemo.immigrant.ontology.entity.eo.teams.repositories.TeamMembershipRepository;
import br.nemo.immigrant.ontology.entity.eo.teams.models.TeamMembership;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.apache.commons.codec.digest.DigestUtils;

@Component
@Transactional
public class TeamMemberApplication {
    @Autowired
    TeamMembershipRepository repository;

    public TeamMembership create (Person person, Team team){

        String name = person.getName() +"-"+ team.getName();

        String internalid = new DigestUtils("SHA3-256").digestAsHex(person.getName()+team.getName());

        TeamMembership teamMembership = TeamMembership.builder().name(name).person(person).team(team).internalId(internalid).build();

        return this.repository.save(teamMembership);
    }

    public Boolean exists (Person person, Team team){
        return this.repository.existsByPersonAndTeam(person,team);
    }
}
