package br.nemo.immigrant.ontology.transformaload.sro.stakeholders.listeners.azuredevops;

import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;
import br.nemo.immigrant.ontology.entity.sro.scrumprocess.models.ScrumDevelopmentTask;
import br.nemo.immigrant.ontology.entity.eo.teams.models.Person;

import br.nemo.immigrant.ontology.entity.eo.teams.repositories.PersonRepository;

import br.nemo.immigrant.ontology.entity.sro.scrumprocess.models.ScrumProject;
import br.nemo.immigrant.ontology.entity.sro.scrumprocess.repositories.ScrumProjectRepository;

import br.nemo.immigrant.ontology.entity.spo.stakeholder.models.ProjectStakeholderSoftwareProject;
import br.nemo.immigrant.ontology.entity.spo.stakeholder.repositories.ProjectStakeholderSoftwareProjectRepository;

import br.nemo.immigrant.ontology.entity.sro.scrumprocess.repositories.ScrumDevelopmentTaskRepository;
import br.nemo.immigrant.ontology.entity.spo.stakeholder.repositories.ProjectPersonStakeholderRepository;
import br.nemo.immigrant.ontology.entity.spo.stakeholder.models.ProjectPersonStakeholder;

import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.application.ScrumProjectApplication;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.exception.ScrumProjectExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.mappers.azuredevops.Project2ScrumProjectElementsMapper;
import br.nemo.immigrant.ontology.transformaload.sro.stakeholders.applications.PersonApplication;
import br.nemo.immigrant.ontology.transformaload.sro.stakeholders.applications.ProjectPersonStakeholderApplication;
import br.nemo.immigrant.ontology.transformaload.sro.stakeholders.mappers.azuredevops.Workitem2PersonMapper;
import br.nemo.immigrant.ontology.transformaload.sro.stakeholders.mappers.azuredevops.Workitem2TeamMapper;
import br.nemo.immigrant.ontology.transformaload.sro.stakeholders.services.PersonService;
import br.nemo.immigrant.ontology.transformaload.sro.util.ApplicationUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.json.simple.JSONObject;

import br.nemo.immigrant.ontology.transformaload.sro.util.ApplicationUtil;

import br.nemo.immigrant.ontology.entity.base.models.Application;

@Slf4j
@RequiredArgsConstructor
@Service
@Transactional
public class PersonWorkitemListener {



    @Autowired
    private PersonService service;

    @Autowired
    private Workitem2PersonMapper personMapper;

    @Autowired
    private Workitem2TeamMapper teamMapper;

    @Autowired
    private Project2ScrumProjectElementsMapper projectMapper;

    private final KafkaTemplate<String, String> kafkaTemplate;

    /** Create Person Stakeholer based on Workitem**/
    @KafkaListener(topics = {"application.msazuredevops.workitem"}, groupId = "createpersonworkitem-group", concurrency = "2")
    public void consume(ConsumerRecord<String, String> payload) {

        try{

            this.service.processList(payload, personMapper,teamMapper, projectMapper );

        }

        catch (Exception e ){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("type", "Exception");
            jsonObject.put("error", e.getMessage());
            jsonObject.put("value", payload.value());

            String jsonString = jsonObject.toJSONString();

            kafkaTemplate.send("application.msazuredevops.workitem.person.error", jsonString);


        }
    }

}


