
package br.nemo.immigrant.ontology.transformaload.sro.stakeholders.listeners.azuredevops;

import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.exception.ScrumProjectExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.mappers.azuredevops.Project2ScrumProjectElementsMapper;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.mappers.azuredevops.Project2ScrumProjectMapper;
import br.nemo.immigrant.ontology.transformaload.sro.stakeholders.services.ScrumTeamService;
import br.nemo.immigrant.ontology.transformaload.sro.stakeholders.filters.azuredevops.Team2ScrumTeamFilter;
import br.nemo.immigrant.ontology.transformaload.sro.stakeholders.mappers.azuredevops.Team2ScrumTeamMapper;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.json.simple.JSONObject;
@Slf4j
@RequiredArgsConstructor
@Service
@Transactional
public class Team2ScrumTeamListener {


    @Autowired
    private Team2ScrumTeamFilter filter;

    @Autowired
    private Team2ScrumTeamMapper teamMapper;

    @Autowired
    private Project2ScrumProjectElementsMapper scrumProjectElementsMapper;


    @Autowired
    private ScrumTeamService service;

    private final KafkaTemplate<String, String> kafkaTemplate;

    /** Create Team*/
    @KafkaListener(topics = "application.msazuredevops.team", groupId = "team2scrumteam-group", concurrency = "2")
    public void consume(ConsumerRecord<String, String> payload) {

        try{

            String data = payload.value();

            if (filter.isValid(data)){

                this.service.process(payload, teamMapper,scrumProjectElementsMapper);
            }

        }

        catch (Exception e ){

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("type", "Exception");
            jsonObject.put("error", e.getMessage());
            jsonObject.put("value", payload.value());

            String jsonString = jsonObject.toJSONString();

            kafkaTemplate.send("application.msazuredevops.team.error", jsonString);

        }
    }
}
