
package br.nemo.immigrant.ontology.transformaload.sro.productsprintbacklog.listeners.jira.issues;

import br.nemo.immigrant.ontology.transformaload.sro.productsprintbacklog.exception.ProductBacklogExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.sro.productsprintbacklog.filters.jira.Issue2AtomicUserStoryFilter;
import br.nemo.immigrant.ontology.transformaload.sro.productsprintbacklog.mappers.jira.Issue2AtomicUserStoryMapper;
import br.nemo.immigrant.ontology.transformaload.sro.productsprintbacklog.services.UserStoryService;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.exception.ScrumProjectExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.mappers.jira.Project2ScrumProjectElementsMapper;
import br.nemo.immigrant.ontology.transformaload.sro.stakeholders.mappers.jira.Workitem2PersonMapper;
import br.nemo.immigrant.ontology.transformaload.sro.stakeholders.mappers.jira.Workitem2TeamMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@RequiredArgsConstructor
@Service
@Transactional
public class Issue2AtomicUserStoryJiraListener {


    @Autowired
    private Issue2AtomicUserStoryFilter filter;

    @Autowired
    private Issue2AtomicUserStoryMapper mapper;

    @Autowired
    private Workitem2PersonMapper personMapper;

    @Autowired
    private Workitem2TeamMapper teamMapper;

    @Autowired
    private Project2ScrumProjectElementsMapper projectMapper;

    @Autowired
    private UserStoryService service;

    private final KafkaTemplate<String, String> kafkaTemplate;

    /** Create a User Story Atomic from Workitem type Product User Story **/
    @KafkaListener(topics = "application.jira.issues", groupId = "issue2atomicuserstory-group", concurrency = "2")
    public void consume(ConsumerRecord<String, String> payload) {

        try{

            String data = payload.value();

            if (filter.isValid(data)){

                this.service.processJira(payload, personMapper,teamMapper, projectMapper, mapper);

            }

        }catch (ProductBacklogExceptionNotFound e){

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("type", "ProductBacklogExceptionNotFound");
            jsonObject.put("error", e.getMessage());
            jsonObject.put("value", payload.value());

            String jsonString = jsonObject.toJSONString();

            kafkaTemplate.send("application.jira.issues.historia.error", jsonString);


        }catch (ScrumProjectExceptionNotFound e){

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("type", "ScrumProjectExceptionNotFound");
            jsonObject.put("error", e.getMessage());
            jsonObject.put("value", payload.value());

            String jsonString = jsonObject.toJSONString();

            kafkaTemplate.send("application.jira.issues.historia.error", jsonString);

        }
        catch (Exception e ){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("type", "Exception");
            jsonObject.put("error", e.getMessage());
            jsonObject.put("value", payload.value());

            String jsonString = jsonObject.toJSONString();

            kafkaTemplate.send("application.jira.issues.historia.error", jsonString);

        }
    }
}
