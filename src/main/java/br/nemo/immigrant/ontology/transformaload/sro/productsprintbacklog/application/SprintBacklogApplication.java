package br.nemo.immigrant.ontology.transformaload.sro.productsprintbacklog.application;

import br.nemo.immigrant.ontology.transformaload.sro.productsprintbacklog.exception.ProductBacklogExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.sro.productsprintbacklog.exception.SprintBacklogExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.exception.ScrumProjectExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.exception.SprintExceptionNotFound;

import br.nemo.immigrant.ontology.entity.sro.productsprintbacklog.repositories.SprintBacklogRepository;
import br.nemo.immigrant.ontology.entity.sro.productsprintbacklog.models.UserStory;
import br.nemo.immigrant.ontology.entity.sro.productsprintbacklog.models.SprintBacklog;
import br.nemo.immigrant.ontology.entity.spo.artifact.models.ArtifactArtifact;
import br.nemo.immigrant.ontology.entity.spo.artifact.repositories.ArtifactArtifactRepository;
import java.util.Optional;
import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Optional;

@Component
@Transactional
public class SprintBacklogApplication {

    @Autowired
    private SprintBacklogRepository repository;

    @Autowired
    private ArtifactArtifactRepository artifactArtifactRepository;

    public ArtifactArtifact create (UserStory userstory, SprintBacklog  sprintBacklog, LocalDateTime eventDate) throws SprintExceptionNotFound {

        ArtifactArtifact artifactArtifact = ArtifactArtifact.builder().artifactfrom(sprintBacklog).artifactto(userstory).eventDate(eventDate).build();

        return this.artifactArtifactRepository.save(artifactArtifact);
    }

    @Transactional
    public SprintBacklog findByName (String sprintName) throws SprintBacklogExceptionNotFound {

        Optional<IDProjection> result = this.repository.findByName(sprintName.trim());
        IDProjection projection = result.orElseThrow(() -> new SprintBacklogExceptionNotFound("Sprint Backlog Not Found "+ sprintName.trim()));
        return createInstance(projection);
    }

    @Transactional
    public SprintBacklog findByInternalId (String internalId) throws SprintBacklogExceptionNotFound {

        Optional<IDProjection> result = this.repository.findByInternalId(internalId.trim());
        IDProjection projection = result.orElseThrow(() -> new SprintBacklogExceptionNotFound("Sprint Backlog Not Found with internalId " + internalId.trim()));
        return createInstance(projection);
    }


    private SprintBacklog createInstance(IDProjection projection){
        return  SprintBacklog.builder().id(projection.getId()).applications(projection.getApplications()).internalId(projection.getInternalId()).name(projection.getName()).build();
    }

    public Boolean exists (UserStory userstory, SprintBacklog  sprintBacklog){
        return  this.artifactArtifactRepository.existsByArtifactfromAndArtifactto(sprintBacklog,userstory);
    }



}
