package br.nemo.immigrant.ontology.transformaload.sro.productsprintbacklog.listeners.jira.issues;


import br.nemo.immigrant.ontology.transformaload.sro.productsprintbacklog.application.UserStoryApplication;
import br.nemo.immigrant.ontology.transformaload.sro.productsprintbacklog.exception.UserStorytExceptionNotFound;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@RequiredArgsConstructor
@Transactional
//@Service("UserStory2UserStoryJiraListener")
public class UserStory2UserStoryListener {

    @Autowired
    UserStoryApplication userStoryApplication;


    private final KafkaTemplate<String, String> kafkaTemplate;

    /** Creating a relationship between  User Story and User Story **/

    @KafkaListener(topics = "application.jira.issues.userstory.related.userstory", groupId = "userstory2userstorylistener-group", concurrency = "2")
    public void consume(ConsumerRecord<String, String> payload) {

        try{

            String data = payload.value();

            ObjectMapper objectMapper = new ObjectMapper();

            JsonNode rootNode = objectMapper.readTree(data);

            if (isValid(rootNode)) {
                String userStoryParentID = rootNode.path("raw").path("fields").path("parent").path("id").asText();
                if (!userStoryParentID.isBlank() && !userStoryParentID.isEmpty()){
                    String externalid = rootNode.path("id").asText();
                    this.userStoryApplication.addUserStory(userStoryParentID,externalid);

                }

            }
        }

        catch (UserStorytExceptionNotFound e){

            log.error("UserStory2UserStoryListener:UserStoryNotFound "+e.getMessage());
            kafkaTemplate.send("application.jira.issues.scrumdevelopmenttask.related.userstory", payload.value());

        }

        catch (Exception e ){
            log.error("UserStory2UserStoryListener:Exception "+e.getMessage());
            kafkaTemplate.send("application.jira.issues.related.userstory.userstory.error", payload.value());
        }
    }



    private Boolean isValid(JsonNode rootNode)  {
        String Value = rootNode.path("raw").path("fields").path("issuetype").path("name").asText().toLowerCase();
        switch (Value) {
            case "epic":
                return true;
            case "história":
                return true;
        }

        return false;

    }

}
