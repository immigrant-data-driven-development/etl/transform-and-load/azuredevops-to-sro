package br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.exception;

public class SprintExceptionNotFound extends RuntimeException{

    public SprintExceptionNotFound(String message) {
        super(message);
    }
}
