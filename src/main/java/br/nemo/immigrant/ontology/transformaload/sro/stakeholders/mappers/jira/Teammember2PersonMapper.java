    package br.nemo.immigrant.ontology.transformaload.sro.stakeholders.mappers.jira;

    import br.nemo.immigrant.ontology.entity.base.models.Application;
    import br.nemo.immigrant.ontology.entity.eo.teams.models.Person;
    import br.nemo.immigrant.ontology.transformaload.sro.util.ApplicationUtil;
    import br.nemo.immigrant.ontology.transformaload.sro.util.Mapper;
    import com.fasterxml.jackson.databind.JsonNode;
    import com.fasterxml.jackson.databind.ObjectMapper;
    import org.apache.commons.codec.digest.DigestUtils;
    import org.springframework.stereotype.Component;

    @Component("Teammember2PersonJiraMapper")
    public class Teammember2PersonMapper implements Mapper<Person> {

    public Person map (String element) throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode rootNode = objectMapper.readTree(element);

        String externalid = rootNode.path("accountId").asText();

        String name = rootNode.path("displayName").asText();

        String email = rootNode.path("emailAddress").asText();

        String internalid = new DigestUtils("SHA3-256").digestAsHex(name);

        Application application = ApplicationUtil.create(externalid,internalid,"jira");
        
        Person person = Person.builder().internalId(internalid).name(name).email(email).build();
        
        person.getApplications().add(application);

        return person;
    }
}
