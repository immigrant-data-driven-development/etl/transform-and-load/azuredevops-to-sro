package br.nemo.immigrant.ontology.transformaload.sro.productsprintbacklog.exception;

public class UserStorytExceptionNotFound extends Exception{

    public UserStorytExceptionNotFound(String message) {
        super(message);
    }
}
