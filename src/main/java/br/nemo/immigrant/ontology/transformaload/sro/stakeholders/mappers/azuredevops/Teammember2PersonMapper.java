    package br.nemo.immigrant.ontology.transformaload.sro.stakeholders.mappers.azuredevops;

    import br.nemo.immigrant.ontology.entity.eo.teams.models.Person;
    import br.nemo.immigrant.ontology.transformaload.sro.util.ApplicationUtil;
    import br.nemo.immigrant.ontology.transformaload.sro.util.Mapper;
    import br.nemo.immigrant.ontology.entity.base.models.Application;
    import org.springframework.stereotype.Component;

    import com.fasterxml.jackson.databind.JsonNode;
    import com.fasterxml.jackson.databind.ObjectMapper;
    import org.apache.commons.codec.digest.DigestUtils;

    @Component
    public class Teammember2PersonMapper  implements Mapper<Person> {

    public Person map (String element) throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode rootNode = objectMapper.readTree(element);

        String externalid = rootNode.path("identity").path("id").asText();

        String name = rootNode.path("identity").path("display_name").asText();

        String email = rootNode.path("identity").path("unique_name").asText();

        String internalid = new DigestUtils("SHA3-256").digestAsHex(name);

        Application application = ApplicationUtil.create(externalid,internalid,"azuredevops");
        
        Person person = Person.builder().internalId(internalid).name(name).email(email).build();
        
        person.getApplications().add(application);

        return person;
    }
}
