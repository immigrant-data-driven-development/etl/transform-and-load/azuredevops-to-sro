package br.nemo.immigrant.ontology.transformaload.sro.productsprintbacklog.application;

import br.nemo.immigrant.ontology.transformaload.sro.productsprintbacklog.exception.ProductBacklogExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.application.ScrumProjectApplication;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.exception.ScrumProjectExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.exception.SprintExceptionNotFound;
import org.springframework.beans.factory.annotation.Autowired;
import br.nemo.immigrant.ontology.entity.base.models.ApplicationNotFound;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import br.nemo.immigrant.ontology.entity.sro.scrumprocess.models.ScrumProject;
import br.nemo.immigrant.ontology.entity.sro.productsprintbacklog.models.ProductBacklog;
import br.nemo.immigrant.ontology.entity.sro.productsprintbacklog.models.UserStory;
import br.nemo.immigrant.ontology.entity.sro.productsprintbacklog.repositories.ProductBacklogRepository;
import br.nemo.immigrant.ontology.entity.sro.scrumprocess.repositories.ScrumProjectRepository;
import br.nemo.immigrant.ontology.entity.sro.scrumprocess.models.ScrumDevelopmentTask;
import br.nemo.immigrant.ontology.entity.spo.artifact.repositories.ArtifactArtifactRepository;
import br.nemo.immigrant.ontology.entity.spo.artifact.models.ArtifactArtifact;
import java.time.LocalDateTime;
import java.util.Optional;
import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;
import br.nemo.immigrant.ontology.entity.spo.process.repositories.ActivityArtifactRepository;
import br.nemo.immigrant.ontology.entity.spo.process.models.ActivityArtifact;
@Component
@Transactional
public class ProductBacklogApplication {

    @Autowired
    private ProductBacklogRepository repository;

    @Autowired
    private ScrumProjectApplication scrumProjectApplication;

    @Autowired
    private ActivityArtifactRepository activityArtifactRepository;

    @Autowired
    private ArtifactArtifactRepository artifactArtifactRepository;

    @Transactional
    public ProductBacklog retrivebyProjectName (String projectName) throws ProductBacklogExceptionNotFound, ScrumProjectExceptionNotFound, ApplicationNotFound, Exception {

        ScrumProject scrumProject = this.scrumProjectApplication.retrieveByName(projectName.trim());
        String externalID = scrumProject.getExternalId("azuredevops");

        Optional<IDProjection> result = this.repository.findByApplicationsExternalId(externalID);

        IDProjection projection = result.orElseThrow(() -> new ProductBacklogExceptionNotFound("Product Backlog Not Found "+ projectName.trim()));
        return createInstance(projection);


    }

    @Transactional
    public ProductBacklog retriveByInternalId (String internalID) throws ProductBacklogExceptionNotFound, Exception {

        Optional<IDProjection> result = this.repository.findByInternalId(internalID);

        IDProjection projection = result.orElseThrow(() -> new ProductBacklogExceptionNotFound("Product Backlog Not Found "+ internalID));
        return createInstance(projection);


    }

    private ProductBacklog createInstance(IDProjection projection){
        return  ProductBacklog.builder().id(projection.getId()).applications(projection.getApplications()).internalId(projection.getInternalId()).name(projection.getName()).build();
    }


    public ActivityArtifact create (ProductBacklog productBacklog, ScrumDevelopmentTask scrumDevelopmentTask){
        ActivityArtifact activityArtifact = ActivityArtifact.builder().artifact(productBacklog).activity(scrumDevelopmentTask).eventDate(scrumDevelopmentTask.getCreatedDate()).build();
        return this.activityArtifactRepository.save(activityArtifact);
    }

    public ArtifactArtifact create (UserStory userstory, ProductBacklog  productBacklog, LocalDateTime eventDate) throws SprintExceptionNotFound {

        ArtifactArtifact artifactArtifact = ArtifactArtifact.builder().artifactfrom(productBacklog).artifactto(userstory).eventDate(eventDate).build();

        return this.artifactArtifactRepository.save(artifactArtifact);
    }

    public Boolean exists (ProductBacklog productBacklog, ScrumDevelopmentTask scrumDevelopmentTask){
        return this.activityArtifactRepository.existsByArtifactAndActivity(productBacklog,scrumDevelopmentTask);
    }

    public Boolean exists (ProductBacklog productBacklog, UserStory userStory){
        return this.artifactArtifactRepository.existsByArtifactfromAndArtifactto(productBacklog,userStory);
    }


}
