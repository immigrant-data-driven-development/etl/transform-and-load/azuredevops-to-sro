    package br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.mappers.azuredevops;

    import br.nemo.immigrant.ontology.entity.sro.scrumprocess.models.ScrumDevelopmentTask;
    import br.nemo.immigrant.ontology.transformaload.sro.util.ApplicationUtil;
    import br.nemo.immigrant.ontology.transformaload.sro.util.DateUtil;
    import br.nemo.immigrant.ontology.transformaload.sro.util.Mapper;
    import br.nemo.immigrant.ontology.transformaload.sro.util.StringUtil;
    import com.fasterxml.jackson.databind.JsonNode;
    import com.fasterxml.jackson.databind.ObjectMapper;
    import org.springframework.stereotype.Component;
    import br.nemo.immigrant.ontology.entity.spo.process.models.StateType;
    import br.nemo.immigrant.ontology.entity.base.models.Application;
    import org.apache.commons.codec.digest.DigestUtils;
    import java.time.LocalDateTime;

    @Component
    public class Workitem2TaskMapper  implements Mapper<ScrumDevelopmentTask> {

    public ScrumDevelopmentTask map (String element) throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode rootNode = objectMapper.readTree(element);

        String externalid = rootNode.path("id").asText();

        String internalid = new DigestUtils("SHA3-256").digestAsHex(externalid);

        String name = rootNode.path("fields").path("System.Title").asText();

        String description = StringUtil.check(rootNode.path("fields").path("System.Description").asText());

        String activity = StringUtil.check(rootNode.path("fields").path("Microsoft.VSTS.Common.Activity").asText());

        String priority = StringUtil.check(rootNode.path("fields").path("Microsoft.VSTS.Common.Priority").asText());

        LocalDateTime createdDate =  DateUtil.createLocalDateTimeZ(rootNode.path("fields").path("System.CreatedDate").asText());

        LocalDateTime updateDate =  DateUtil.createLocalDateTimeZ(rootNode.path("fields").path("System.RevisedDate").asText());

        String tags = StringUtil.check(rootNode.path("fields").path("System.Tags").asText());

        Application application = ApplicationUtil.create(externalid,internalid,"azuredevops");

        ScrumDevelopmentTask scrumDevelopmentTask = ScrumDevelopmentTask.builder().
                internalId(internalid).
                name(name).
                description(description).
                tags(tags).
                createdDate(createdDate).
                updatedDate(updateDate).
                activityType(activity).
                priority(priority).
                build();

        scrumDevelopmentTask.getApplications().add(application);
        return scrumDevelopmentTask;
    }
}
