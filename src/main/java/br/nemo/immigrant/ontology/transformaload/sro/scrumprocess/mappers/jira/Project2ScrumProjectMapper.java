    package br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.mappers.jira;

    import br.nemo.immigrant.ontology.entity.base.models.Application;
    import br.nemo.immigrant.ontology.entity.sro.scrumprocess.models.ScrumProject;
    import br.nemo.immigrant.ontology.transformaload.sro.util.ApplicationUtil;
    import br.nemo.immigrant.ontology.transformaload.sro.util.Mapper;
    import com.fasterxml.jackson.databind.JsonNode;
    import com.fasterxml.jackson.databind.ObjectMapper;
    import org.apache.commons.codec.digest.DigestUtils;
    import org.springframework.stereotype.Component;

    @Component("Project2ScrumProjectJiraMapper")
    public class Project2ScrumProjectMapper implements Mapper<ScrumProject> {

    public ScrumProject map (String element) throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode rootNode = objectMapper.readTree(element);

        String name = rootNode.path("name").asText();

        String externalid = rootNode.path("uuid").asText();

        String url = rootNode.path("self").asText();

        String internalid = new DigestUtils("SHA3-256").digestAsHex(externalid);
        
        Application application = ApplicationUtil.create(externalid,internalid,"jira");

        ScrumProject scrumProject = ScrumProject.builder().name(name).
                internalId(internalid).
                url(url).
                build();

        scrumProject.getApplications().add(application);

        return scrumProject;
    }
}
