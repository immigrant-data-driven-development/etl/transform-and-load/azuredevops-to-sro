package br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.listeners.azuredevops.workitem;


import br.nemo.immigrant.ontology.transformaload.sro.productsprintbacklog.exception.UserStorytExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.application.ScrumDevelopmentTaskApplication;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.exception.ScrumDevelopmentTaskExceptionNotFound;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@RequiredArgsConstructor
@Service
@Transactional
public class DevelopmentTask2UserStoryListener {

    @Autowired
    ScrumDevelopmentTaskApplication scrumDevelopmentTaskApplication;
    private final KafkaTemplate<String, String> kafkaTemplate;

    /** Creating a relationship between  Development Task and User Story **/
    @KafkaListener(topics = "application.msazuredevops.workitem.scrumdevelopmenttask.related.userstory", groupId = "developmenttask2userstorylistener-group", concurrency = "2")
    public void consume(ConsumerRecord<String, String> payload) {

        try{

            String data = payload.value();

            ObjectMapper objectMapper = new ObjectMapper();

            JsonNode rootNode = objectMapper.readTree(data);


            if (isValid(rootNode)) {

                String userStoryID = rootNode.path("fields").path("System.Parent").asText();
                if (!userStoryID.isBlank() && !userStoryID.isEmpty()){

                    String scrumDevelopmentTaskExternalid = rootNode.path("id").asText();
                    this.scrumDevelopmentTaskApplication.addUserStory(userStoryID,scrumDevelopmentTaskExternalid);

                }

            }
        }

        catch (ScrumDevelopmentTaskExceptionNotFound e){

            log.error("DevelopmentTask2UserStoryListener:ScrumDevelopmentTaskExceptionNotFound "+e.getMessage());

            kafkaTemplate.send("application.msazuredevops.workitem.related.scrumdevelopmenttask.userstory", payload.value());
        }
        catch (UserStorytExceptionNotFound e){

            log.error("DevelopmentTask2UserStoryListener:UserStoryNotFound "+e.getMessage());
            kafkaTemplate.send("application.msazuredevops.workitem.related.scrumdevelopmenttask.userstory", payload.value());

        }

        catch (Exception e ){
            log.error("DevelopmentTask2UserStoryListener:Exception "+e.getMessage());
            kafkaTemplate.send("application.msazuredevops.workitem.related.scrumdevelopmenttask.userstory.error", payload.value());
        }
    }



    private Boolean isValid(JsonNode rootNode) throws Exception {

       return rootNode.path("fields").path("System.WorkItemType").asText().equals("Task") ? true: false;

    }

}
