package br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.application;


import br.nemo.immigrant.ontology.entity.sro.scrumprocess.models.ScrumDevelopmentTask;
import br.nemo.immigrant.ontology.entity.sro.scrumprocess.models.ScrumProject;
import br.nemo.immigrant.ontology.entity.sro.scrumprocess.repositories.ScrumDevelopmentTaskRepository;
import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;

import org.apache.commons.codec.digest.DigestUtils;
import java.time.LocalDateTime;
import java.util.Optional;
import br.nemo.immigrant.ontology.entity.sro.productsprintbacklog.models.UserStory;
import br.nemo.immigrant.ontology.transformaload.sro.productsprintbacklog.application.UserStoryApplication;
import br.nemo.immigrant.ontology.transformaload.sro.productsprintbacklog.exception.UserStorytExceptionNotFound;
import br.nemo.immigrant.ontology.transformaload.sro.scrumprocess.exception.ScrumDevelopmentTaskExceptionNotFound;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import br.nemo.immigrant.ontology.entity.spo.process.repositories.ActivityArtifactRepository;
import br.nemo.immigrant.ontology.entity.spo.process.models.ActivityArtifact;

import br.nemo.immigrant.ontology.entity.spo.process.models.ActivityEvent;
import br.nemo.immigrant.ontology.entity.spo.process.models.StateType;
import br.nemo.immigrant.ontology.entity.spo.process.models.SuccessType;
import br.nemo.immigrant.ontology.entity.spo.process.repositories.ActivityEventRepository;
@Component
@Transactional
public class ScrumDevelopmentTaskApplication {

    @Autowired
    private ScrumDevelopmentTaskRepository repository;

    @Autowired
    private ActivityArtifactRepository activityArtifactRepository;

    @Autowired
    private ActivityEventRepository activityEventRepository;

    @Autowired
    UserStoryApplication userStoryApplication;

    public ScrumDevelopmentTask create (ScrumDevelopmentTask scrumDevelopmentTask, StateType stateType, SuccessType successType){

        scrumDevelopmentTask = this.repository.save(scrumDevelopmentTask);

        ActivityEvent activityEvent = ActivityEvent.builder().activity(scrumDevelopmentTask).eventDate(scrumDevelopmentTask.getCreatedDate()).statetype(stateType).successtype(successType).build();

        activityEventRepository.save(activityEvent);

        return scrumDevelopmentTask;


    }

    public ScrumDevelopmentTask update(ScrumDevelopmentTask scrumDevelopmentTask) throws ScrumDevelopmentTaskExceptionNotFound {
        ScrumDevelopmentTask existingScrumDevelopmentTask = this.retrieve(scrumDevelopmentTask.getInternalId());
        Optional<ScrumDevelopmentTask> optionalTask = this.repository.findById(existingScrumDevelopmentTask.getId());

        if (optionalTask.isEmpty()) {
            throw new RuntimeException("Task not found with ID: " + existingScrumDevelopmentTask.getId());
        }

        existingScrumDevelopmentTask = optionalTask.get();

        existingScrumDevelopmentTask.setDescription(scrumDevelopmentTask.getDescription());
        existingScrumDevelopmentTask.setName(scrumDevelopmentTask.getName());
        existingScrumDevelopmentTask.setUpdatedDate(scrumDevelopmentTask.getUpdatedDate());
        existingScrumDevelopmentTask.setCreatedDate(scrumDevelopmentTask.getCreatedDate());
        existingScrumDevelopmentTask.setPriority(scrumDevelopmentTask.getPriority());
        existingScrumDevelopmentTask.setStartDate(scrumDevelopmentTask.getStartDate());
        existingScrumDevelopmentTask.setEndDate(scrumDevelopmentTask.getEndDate());

        existingScrumDevelopmentTask = this.repository.save(existingScrumDevelopmentTask);

        return existingScrumDevelopmentTask;
    }

    public Boolean exists (String internalId){
        return repository.existsByInternalId(internalId);
    }

    public ScrumDevelopmentTask retrieve(String internalID) throws ScrumDevelopmentTaskExceptionNotFound {
        Optional<IDProjection> result = this.repository.findByInternalId(internalID);

        IDProjection projection = result.orElseThrow(() -> new ScrumDevelopmentTaskExceptionNotFound());

        return ScrumDevelopmentTask.builder().id(projection.getId()).build();
    }

    public void addUserStory (String userStoryInternalID, String scrumDevelopmentTaskInternalID) throws UserStorytExceptionNotFound, ScrumDevelopmentTaskExceptionNotFound{

        //scrumDevelopmentTaskExternalID = new DigestUtils("SHA3-256").digestAsHex(scrumDevelopmentTaskExternalID);
        //userStoryExternalID =  new DigestUtils("SHA3-256").digestAsHex(userStoryExternalID);
        ScrumDevelopmentTask scrumDevelopmentTask = this.retrieve(scrumDevelopmentTaskInternalID);

        UserStory userStory = this.userStoryApplication.retrieve(userStoryInternalID);

        //Relacionar um artefato com uma tarefa

        Boolean exists =  this.activityArtifactRepository.existsByArtifactAndActivity(userStory,scrumDevelopmentTask);

        if (!exists){

            ActivityArtifact instance = ActivityArtifact.builder().artifact(userStory).activity(scrumDevelopmentTask).build();
            this.activityArtifactRepository.save(instance);
        }



    }
}
