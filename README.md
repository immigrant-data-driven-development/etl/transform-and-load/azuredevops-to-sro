
# Transform and Load from Microsoft Azure DevOps to SRO
## 🚀 Goal

Mapping MS AzureDevOps Model to SRO Database.

## 📕 Domain Documentation

Domain documentation can be found [here](./docs/README.md)

## 📕 Design Documentation

Design documentation can be found [here](./docs/README.md)


## ⚙️ Requirements

1. Postgresql
2. Java 17
3. Maven

## ⚙️ Stack

1. Spring Boot 3.0
2. Spring Kafka

# Usage
Execute the follow command: 

```bash
mvn spring-boot:run
```

Execute the follow command: 

```bash
docker-compose up 
```

## ✒️ Team

* **[Paulo Sérgio dos Santos Júnior](paulossjunior@gmail.com)**

## 📕 Literature

