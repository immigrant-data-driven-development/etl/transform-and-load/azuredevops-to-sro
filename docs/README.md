# 📕Documentation

## 🌀 Project's Package Model
![Domain Diagram](packagediagram.png)

### 📲 Modules
* **[ScrumProcess](./scrumprocess/)** :Subontology addresses the events that occur in a project that adopts Scrum, such as the Scrum ceremonies
* **[ProductSprintBacklog](./productsprintbacklog/)** :Addresses aspects related to the requirements established in a Scrum project and activities planned to materialize them
* **[Stakeholders](./stakeholders/)** :Concerns the teams, agents and roles involved in a Scrum project

