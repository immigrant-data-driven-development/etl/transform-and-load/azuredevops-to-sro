# 📕Documentation: ProductSprintBacklog
Addresses aspects related to the requirements established in a Scrum project and activities planned to materialize them

## 🌀 Package's Data Model
![Domain Diagram](classdiagram.png)

### ⚡Entities

* **UserStory** : A Requirement Artifact that describes Requirements in a Scrum Project.

## ✒️ Mapping Rules
### UserStory
A Requirement Artifact that describes Requirements in a Scrum Project.

#### From workitem to UserStory

* *id* **equal** *externalId*
* *internal_uuid* **equal** *internalId*
* *fields.System.Title* **equal** *name*

**When** *fields.System.WorkItemType* **equal** *Epic*

#### From workitem to UserStory

* *id* **equal** *externalId*
* *internal_uuid* **equal** *internalId*
* *fields.System.Title* **equal** *name*

**When** *fields.System.WorkItemType* **equal** *Product Backlog Item*

#### From workitem to UserStory

* *id* **equal** *externalId*
* *internal_uuid* **equal** *internalId*
* *fields.System.Title* **equal** *name*

**When** *fields.System.WorkItemType* **equal** *User Story*


