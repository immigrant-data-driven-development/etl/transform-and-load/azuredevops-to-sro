# 📕Documentation: Stakeholders
Concerns the teams, agents and roles involved in a Scrum project

## 🌀 Package's Data Model
![Domain Diagram](classdiagram.png)

### ⚡Entities

* **Person** : A Person that performs a role in a scrum project
* **ScrumTeam** : A Team in a Scrum project

## ✒️ Mapping Rules
### Person
A Person that performs a role in a scrum project

#### From teammember to Person

* *id* **equal** *externalId*
* *internal_uuid* **equal** *internalId*
* *display_name* **equal** *name*
* *unique_name* **equal** *email*

### ScrumTeam
A Team in a Scrum project

#### From team to ScrumTeam

* *id* **equal** *externalId*
* *internal_uuid* **equal** *internalId*
* *name* **equal** *name*
* *description* **equal** *description*




